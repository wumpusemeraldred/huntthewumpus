﻿using HuntTheWumpus.Controllers.UI_Stuff;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using HuntTheWumpus.Battle.Entities;
using HuntTheWumpus.Battle.Entities.Players;
using HuntTheWumpus.Battle.Equipment;

namespace HuntTheWumpus
{
    public partial class BattleForm : Form
    {
        //battle instance variables
        private Boolean[] playersAlive = new Boolean[3];
        private int[] droppedItems = new int[50];
        private Boolean[] playersTurnTaken = new Boolean[3];
        private int expEarned = 0;
        private int APEarned = 0;
        private int goldEarned = 0;
        private int scoreEarned = 0;
        private FoePartyList foePartyList;
        private Foe[] foes;
        private PlayerCharacter[] players;
        private PlayerCharacter selectedPlayer;

        //graphics
        private BattleMenu TurnMenu;//keeps track of variables for turn
        private Boolean turnOver;

        //Used to draw on
        private Bitmap background;
        private Bitmap buffer;
        private Bitmap foreground;
        //used to remove lingering draws
        private Rectangle pastRect;
        //Graphics for menu
        private Sprite mainBattleMenu;
        private Sprite battleSkillSub;
        private Sprite battleMenu1;
        private Sprite battleSelection;
        private Sprite Backdrop;

        private int CurrentRender; //sets rendered rectangle, prevents flickering
        private int pastSelection; //renders previous tile
        private int tick; //keeps track of frame
        private int currentFrame; //keeps track of frame animation

        //dimensions for panel
        private int dispWidth;
        private int dispHeight;



        private Rectangle[] rectList;


        public BattleForm(int playerOneLevel, int playerTwoLevel, int playerThreeLevel, int foePartyType)
        {
            InitializeComponent();
            TurnMenu = new BattleMenu(); //initializes player menu

            //getting sprites ready for use

            mainBattleMenu = new Sprite(".\\Images\\MainBattleMenuSpriteSheet.png", 4, 64 * 3, 36 * 3, new Point((pnlDisplay.Width * 1) / 16, (pnlDisplay.Height * 6) / 8));
            battleSkillSub = new Sprite(".\\Images\\SkillSubBattleMenuSpriteSheet.png", 3, 64 * 3, 36 * 3, new Point((pnlDisplay.Width * 4) / 16, (pnlDisplay.Height * 6) / 8));
            battleMenu1 = new Sprite(".\\Images\\BlankBattleMenuSpriteSheet.png", 4, 64 * 3, 36 * 3, new Point((pnlDisplay.Width * 4) / 16, (pnlDisplay.Height * 6) / 8));
            battleSelection = new Sprite(".\\Images\\TargetSpriteSheet.png", 8, 72 * 3, 72 * 3, new Point((pnlDisplay.Width * 4) / 7, (pnlDisplay.Height * 6) / 8));
            players = new PlayerCharacter[1];
            players[0] = new Paul();
            //players[1] = new Emma();
            //players[2] = new Matthew();


            // initializes party
            foePartyList = new FoePartyList();
            foes = foePartyList.getFoeParty(foePartyType);

            foreach (Foe f in foes) //gives all foes the player array
            {
                f.setPlayers(players);
            }
            foreach (PlayerCharacter p in players) // gives all players the player array and foe list
            {
                p.setPlayers(players);
                p.setFoes(foes);
            }

            CurrentRender = 0;
            tick = 0;
            currentFrame = 0;
            pastSelection = TurnMenu.getSubSelection();

            pastRect = new Rectangle(battleMenu1.getPoint(), battleMenu1.getSize()); //initializes first rectangle for invalidation

            pnlDisplay.BackColor = Color.Transparent;

            dispWidth = pnlDisplay.Width;
            dispHeight = pnlDisplay.Height;

            int dispMidX = dispWidth / 2;
            int dispMidY = dispHeight / 2;

            //Graphic layers
            foreground = new Bitmap(dispWidth, dispHeight);
            buffer = new Bitmap(dispWidth, dispHeight); //off-screen drawing, helps update panel faster
            background = new Bitmap(dispWidth, dispHeight);

            //initializes image for background
            Graphics Background = Graphics.FromImage(background);
            Background.DrawImage(new Bitmap(".\\Images\\RedCaveBackground.png"), new Rectangle(0, 0, dispWidth, dispHeight));

            //"                   " foreground
            Graphics Foreground = Graphics.FromImage(foreground);
            Foreground.DrawImage(new Bitmap(".\\Images\\RedCaveForeground.png"), new Rectangle(0, 0, dispWidth, dispHeight));

            //puts the two together
            Graphics g = Graphics.FromImage(buffer);
            g.DrawImage(background, new Rectangle(0, 0, dispWidth, dispHeight));
            g.DrawImage(foreground, new Rectangle(0, 0, dispWidth, dispHeight));

            turnOver = false;
            this.Focus();
        }


        //graphics stuff
        private void Key(object sender, KeyEventArgs e) //gets player input for navigating menus
        {
            if (TurnMenu.getFill() && e.KeyCode == Keys.Right)
            {
                turnOver = true;
                //pnlDisplay.Invalidate(rectList[TurnMenu.getMenuOutput(2)]);
            }
            TurnMenu.shiftSelection(sender, e);

            TurnMenu.displayMenuOutput(Selection1);
            label2.Text = players.Length.ToString() + "Length";

        }

        private void timer1_Tick(object sender, EventArgs e) //keeps track of time
        {
            if (tick == 0)
            {
                label2.Text = "Timer1";
                TurnMenu = new BattleMenu();
                turnOver = false;
                CurrentRender = 0;
            }

            //Off-screen Rendering
            Rectangle mainBattleMenuRect = new Rectangle(mainBattleMenu.getPoint(), mainBattleMenu.getSize());
            Rectangle battleMenu1Rect = new Rectangle(battleMenu1.getPoint(), battleMenu1.getSize());
            Rectangle battleMenu2Rect = new Rectangle(battleSkillSub.getPoint(), battleSkillSub.getSize());
            Rectangle battleSelectionRect = new Rectangle(battleSelection.getPoint(), battleSelection.getSize());
            rectList = new Rectangle[players.Length];
            for (int i = 0; i < players.Length; i++)
            {
                rectList[i] = new Rectangle(new Point(0, 0), players[i].getSprite().getSize());
            }

            if (tick % 6 == 0 && TurnMenu.getSubSelection() == 2) //spinning target sprite for enemy/player selection
            {
                currentFrame++;
                currentFrame = currentFrame % 8;
            }
            battleSelection.setPoint(battleSelection.getPoint().X, (TurnMenu.getSelectionOfSubSelection(2) + 1) * 72); //manages pixel location for targets

            //drawing
            using (Graphics g = Graphics.FromImage(buffer))
            {
                drawSprite(g, mainBattleMenuRect);
                drawSprite(g, battleMenu2Rect);
                drawSprite(g, battleMenu1Rect);
                drawSprite(g, battleSelectionRect);
                drawSprite(g, pastRect);

                foreach (Rectangle rect in rectList)
                {
                    drawSprite(g, rect);
                }
                for (int i = 0; i < players.Length; i++)
                {
                    players[i].getSprite().renderTile(g, 0, rectList[i]);
                }

                //receives keyboard input from TurnMenu and reacts accordingly
                if (TurnMenu.getSubSelection() == 0)
                {
                    mainBattleMenu.renderTile(g, TurnMenu.getSelectionOfSubSelection(0), mainBattleMenuRect);
                }

                if (TurnMenu.getSubSelection() >= 1 && TurnMenu.getMenuOutput(0) == 2)
                {
                    battleSkillSub.renderTile(g, TurnMenu.getSelectionOfSubSelection(1), battleMenu1Rect);
                }

                else if (TurnMenu.getSubSelection() >= 1 && TurnMenu.getMenuOutput(0) != 0)
                {
                    battleMenu1.renderTile(g, TurnMenu.getSelectionOfSubSelection(1), battleMenu1Rect);
                }


                if (TurnMenu.getSubSelection() == 2)
                {
                    battleSelection.renderTile(g, currentFrame, battleSelectionRect);
                }
                CurrentRender = TurnMenu.getSubSelection();
                if (turnOver)
                {
                    drawSprite(g, mainBattleMenuRect);
                    drawSprite(g, battleMenu2Rect);
                    drawSprite(g, battleMenu1Rect);
                    drawSprite(g, battleSelectionRect);
                    drawSprite(g, pastRect);
                    pastSelection = TurnMenu.getSubSelection();
                }
            }
            if (TurnMenu.getSubSelection() != pastSelection)
            {
                pnlDisplay.Invalidate(pastRect);
                pastSelection = TurnMenu.getSubSelection();
            }
            if (CurrentRender == 0)
            {
                pnlDisplay.Invalidate(mainBattleMenuRect);
            }
            if (CurrentRender == 1)
            {
                pnlDisplay.Invalidate(battleMenu1Rect);
                pastRect = new Rectangle(battleMenu1.getPoint(), battleMenu1.getSize());
            }
            if (CurrentRender == 2)
            {
                pnlDisplay.Invalidate(battleSelectionRect);
                pnlDisplay.Invalidate(pastRect);
                pastRect = new Rectangle(battleSelection.getPastPoint(), battleSelection.getSize());
            }

            tick++;
            if (turnOver)
            {
                foreach (Rectangle rect in rectList)
                {
                    pnlDisplay.Invalidate(rect);
                }
                timer1.Enabled = false;
                tick = 0;
                timer2.Enabled = true;
                pnlDisplay.Invalidate(new Rectangle(0, 0, dispWidth, dispHeight));
            }
        }

        private void timer2_Tick(object sender, EventArgs e) //combat sequence happens here
        {
            //int totalEntities = players.Length + foeParty.Length;
            if (tick == 0)
            {
                foreach (PlayerCharacter p in players) //update buffs and status effects
                {
                    if (p.getAlive())
                    {
                        p.updateBuffsAndStatusEffects();
                        p.canTakeTurn = true;
                    }
                }
                label2.Text = "Timer2";
                label2.Text += "Timer2";

            }
            if(tick < players.Length)
            {
                if (players[tick].getAlive() && players[tick].canTakeTurn)
                {
                    /*
                int temp = tick;
                    
                    timer2.Enabled = false;
                tick = 0;
                    timer1.Enabled = true;
                    

                    this.checkAlive();
                tick = temp;*/
                    players[tick].takeAction(TurnMenu.getMenuOutput());
                    players[tick].canTakeTurn = false;
                }
                else
                {
                    //timer2.Enabled = true;
                }
            }
            //if(timer2.Enabled)
            //{

                using (Graphics g = Graphics.FromImage(buffer))
                {
                }
           
                if (tick == players.Length) //when players have finished taking their turns
                {
                    foreach (Foe f in foes) //update buffs and status effects for all enemies
                    {
                        f.updateBuffsAndStatusEffects();
                        f.canTakeTurn = true;
                    }
                }

                if (tick < foes.Length + players.Length && tick >= players.Length)
                {
                    if(foes[tick - players.Length].alive && foes[tick - players.Length].canTakeTurn)
                    {
                        foes[tick - players.Length].takeTurn();
                    }
                
                    hpLabel.Text = players[0].HP.ToString() + " / " + players[0].maxHP.ToString();
                    Enemy1HP.Text = foes[0].HP.ToString() + " / " + foes[0].maxHP.ToString();
                    Enemy2HP.Text = foes[1].HP.ToString() + " / " + foes[1].maxHP.ToString();
                    Enemy3HP.Text = foes[2].HP.ToString() + " / " + foes[2].maxHP.ToString();
                    this.checkAlive();
                }
            //}
            tick++;

            if (tick == foes.Length + players.Length)
            {
                    timer2.Enabled = false;
                    tick = 0;
                    timer1.Enabled = true;
            }

        }

        private void pnlDisplay_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(buffer, e.ClipRectangle, e.ClipRectangle, GraphicsUnit.Pixel);
        }
        private void drawSprite(Graphics g, Rectangle rect)
        {
            g.DrawImage(background, rect, rect, GraphicsUnit.Pixel);
            g.DrawImage(foreground, rect, rect, GraphicsUnit.Pixel);
        }
        public int[] getDrops()
        {
            return droppedItems;
        }

        private void BattleForm_Load(object sender, EventArgs e)
        {

        }

        //gameplay methods
        public void checkAlive() //check after each turn which enemies and players are alive
        {
            for (int i = 0; i < foes.Length; i++)
            {
                if (foes[i].HP == 0) //if the foe has died, get all the drops from this enemy
                {
                    foes[i].disappear(); //gets drops
                    for (int j = 0; j < foes[i].getFinalItemDrops().Length; j++) //adds drops to the total drops attached to this instance of Battlefield
                    {
                        droppedItems[j] += foes[i].getFinalItemDrops()[j];
                    }

                    //gets added scores for this instance
                    scoreEarned += foes[i].getScore();
                    expEarned += foes[i].getEXP();
                    APEarned += foes[i].getAP();
                    goldEarned += foes[i].getGold();
                    foes[i].alive = false; //enemy will be set to dead
                }
            }
            for (int i = 0; i < players.Length; i++)
            {
                if (!players[i].getAlive())
                {
                    players[i].resetBuffsAndStatusEffects();
                    //sprite change to dead
                }
            }
        }

        public int getEXP()
        {
            return expEarned;
        }

        public int getAP()
        {
            return APEarned;
        }

        public int getGold()
        {
            return goldEarned;
        }

        public int getScore()
        {
            return scoreEarned;
        }

        public int[] getItemsDropped()
        {
            return droppedItems;
        }

        private void hpLabel_Click(object sender, EventArgs e)
        {

        }
    }
}
