﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HuntTheWumpus
{
    public partial class HighScoreForm : Form
    {
        private HighScore highscore;
        public HighScoreForm(HighScore score)
        {
            InitializeComponent();
            highscore = score;
        }

        private void EnteredScore_ValueChanged(object sender, EventArgs e)
        {
            highscore.SetHighScore((int)EnteredScore.Value);
        }

        private void HighScoreReceiver_TextChanged(object sender, EventArgs e)
        {
            //int HighScore = this.Text;
        }
    }
}
