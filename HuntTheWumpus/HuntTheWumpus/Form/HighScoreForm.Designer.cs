﻿namespace HuntTheWumpus
{
    partial class HighScoreForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.EnteredScore = new System.Windows.Forms.NumericUpDown();
            this.HighScoreReceiver = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.EnteredScore)).BeginInit();
            this.SuspendLayout();
            // 
            // EnteredScore
            // 
            this.EnteredScore.Location = new System.Drawing.Point(104, 158);
            this.EnteredScore.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.EnteredScore.Name = "EnteredScore";
            this.EnteredScore.Size = new System.Drawing.Size(180, 26);
            this.EnteredScore.TabIndex = 0;
            this.EnteredScore.ValueChanged += new System.EventHandler(this.EnteredScore_ValueChanged);
            // 
            // HighScoreReceiver
            // 
            this.HighScoreReceiver.AcceptsReturn = true;
            this.HighScoreReceiver.Location = new System.Drawing.Point(104, 88);
            this.HighScoreReceiver.Name = "HighScoreReceiver";
            this.HighScoreReceiver.ReadOnly = true;
            this.HighScoreReceiver.Size = new System.Drawing.Size(100, 26);
            this.HighScoreReceiver.TabIndex = 1;
            this.HighScoreReceiver.TextChanged += new System.EventHandler(this.HighScoreReceiver_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(104, 124);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 26);
            this.textBox2.TabIndex = 2;
            // 
            // HighScoreForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(426, 402);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.HighScoreReceiver);
            this.Controls.Add(this.EnteredScore);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "HighScoreForm";
            this.Text = "HighScoreForm";
            ((System.ComponentModel.ISupportInitialize)(this.EnteredScore)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown EnteredScore;
        private System.Windows.Forms.TextBox HighScoreReceiver;
        private System.Windows.Forms.TextBox textBox2;
    }
}