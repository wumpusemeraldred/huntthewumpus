﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HuntTheWumpus
{
    public partial class TriviaForm : Form
    {
        private Trivia Trivia;

        public TriviaForm(Trivia trivia)
        {
            InitializeComponent();
            Trivia = trivia;

            Question q = trivia.PickQuestion();
            this.labelQuestion.Text = q.question;
            this.buttonA.Text = q.answerA;
            this.buttonB.Text = q.answerB;
            this.buttonC.Text = q.answerC;
            this.buttonD.Text = q.answerD;
        }

        private void buttonA_Click(object sender, EventArgs e)
        {
            Trivia.SetUsersAnswer("A");
            this.Close();
        }

        private void buttonB_Click(object sender, EventArgs e)
        {
            Trivia.SetUsersAnswer("B");
            this.Close();
        }

        private void buttonC_Click(object sender, EventArgs e)
        {
            Trivia.SetUsersAnswer("C");
            this.Close();
        }

        private void buttonD_Click(object sender, EventArgs e)
        {
            Trivia.SetUsersAnswer("D");
            this.Close();
        }

        private void TriviaForm_Load(object sender, EventArgs e)
        {

        }

        private void labelQuestion_Click(object sender, EventArgs e)
        {

        }
    }
}
