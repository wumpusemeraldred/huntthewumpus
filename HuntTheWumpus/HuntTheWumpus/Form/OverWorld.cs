﻿using HuntTheWumpus.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HuntTheWumpus.Battle.Entities.Players;
using HuntTheWumpus.Battle.Entities;
using HuntTheWumpus.Controllers.UI_Stuff;
using HuntTheWumpus.Battle;


namespace HuntTheWumpus
{
    public partial class OverWorld : Form
    {

        private Map mapData;
        private MapPosition position;
        private MapPosition playerPos;
        private Room[] myRooms;
        private Room currentRoom;
        private RoomGraphics roomGraphics;

        private Point roomRangeX;
        private Point roomRangeY;

        private Sprite rocky;
        private Sprite rocky2;

        private Bitmap background;
        private Bitmap obsticles;
        private Bitmap buffer;
        private Bitmap foreground;

        private Graphics Background;
        private Graphics g;
        private Graphics Foreground;

        private string str;

        private int[] walls;
        private int m1_CurrentFrame = 0;
        private int m2_CurrentFrame = 1;
        private int counter;
        private int dispWidth;
        private int dispHeight;

        private Boolean move=false;

        private Sprite[][] mapSprites;
        private int[,] mapObstacles;

        private Point end;
        private Point range;
        private Point start;
        private int previousRoom;
        private InventoryData inventory;
        


        public OverWorld()
        {
            InitializeComponent();

            //initializing for map
            mapData = new Map();
            mapData.InitializeDefaultMap();
            playerPos = mapData.GetPlayerPosition();
            myRooms = mapData.GetRooms();
            currentRoom = myRooms[playerPos.RoomNumber];

            mapObstacles=new int [5,5];

            inventory = new InventoryData(); //keeps track of most gameplay elements

            previousRoom = -1;

            walls = mapData.GetCurrentRoomOpenWalls();//mapData.GetWalls();//walls:0 = top, 1 = right, 2 = left, 3 = bottom)

            rocky =new Sprite(".\\Images\\WalkingSpriteSheetV2.png", 20, 640*3, 32*3,
                new Point((pnlDisplay.Width/7 )* (mapData.GetPlayerPosition().XCoordinate+1), (pnlDisplay.Height / 7 )* (mapData.GetPlayerPosition().YCoordinate+1)));
            //rocky = new Sprite(".\\Images\\WalkingSpriteSheet.png", 20, 640 * 3, 32 * 3, new Point((pnlDisplay.Width / 7) * 3, (pnlDisplay.Height / 7) * 3));
            rocky2 = new Sprite(".\\Images\\RockSpriteSheet.png", 4, 128, 32);

            pnlDisplay.BackColor=Color.Transparent;



            dispWidth = pnlDisplay.Width;
            dispHeight = pnlDisplay.Height;

            int dispMidX = (int)dispWidth / 2;
            int dispMidY = (int)dispHeight / 2;

            //initial renders for background and foreGround
            roomGraphics = new RoomGraphics(dispWidth,dispHeight);
            foreground = new Bitmap(dispWidth, dispHeight);
            buffer = new Bitmap(dispWidth, dispHeight);
            background = new Bitmap(dispWidth, dispHeight);

            Graphics Background = Graphics.FromImage(background);
            roomGraphics.DrawBackground(Background, walls);
            Graphics Foreground = Graphics.FromImage(foreground);
            roomGraphics.DrawForeground(Foreground, walls);

            Graphics g = Graphics.FromImage(buffer);
            g.DrawImage(background, new Rectangle(0, 0, dispWidth, dispHeight));
            g.DrawImage(foreground, new Rectangle(0, 0, dispWidth, dispHeight));

            this.Focus();
        }



        private void timer1_Tick(object sender, EventArgs e)
        {
            if (previousRoom != mapData.GetPlayerPosition().RoomNumber)
            {
                //get room content
                for (int i = 0; i < 5; i++)
                {
                    for (int j = 0; j < 5; j++)
                    {
                        int num;
                        if (currentRoom.cellContents[i, j].GetType() == typeof(Obstacle))
                        {
                            num = 1;
                        }
                        if (currentRoom.cellContents[i, j].GetType() == typeof(Treasure))
                        {
                            num = 2;
                        }
                        if (currentRoom.cellContents[i, j].GetType() == typeof(Enemy))
                        {
                            num = 3;
                        }
                        else
                        {
                            num = 0;
                        }
                        mapObstacles[i, j] = num;
                    }
                }
                
            walls = mapData.GetCurrentRoomOpenWalls();

                //Blank Sheets For Drawing
            roomGraphics = new RoomGraphics(dispWidth,dispHeight);
            foreground = new Bitmap(dispWidth, dispHeight);
            buffer = new Bitmap(dispWidth, dispHeight);
            background = new Bitmap(dispWidth, dispHeight);
            obsticles = new Bitmap(dispWidth, dispHeight);

            Background = Graphics.FromImage(background);
            roomGraphics.DrawBackground(Background, walls);
            roomGraphics.DrawObstacles(Background, mapObstacles);

            Foreground = Graphics.FromImage(foreground);
            roomGraphics.DrawForeground(Foreground, walls);

            g = Graphics.FromImage(buffer);
            g.DrawImage(background, new Rectangle(0, 0, dispWidth, dispHeight));
            g.DrawImage(foreground, new Rectangle(0, 0, dispWidth, dispHeight));

                previousRoom = mapData.GetPlayerPosition().RoomNumber;
            pnlDisplay.Invalidate();
            }
            /*
            label1.Text = mapData.GetPlayerPosition().XCoordinate.ToString()+" , "+mapData.GetPlayerPosition().YCoordinate.ToString()+" , "+mapData.GetPlayerPosition().RoomNumber.ToString();
            label1.Text += "\n" + start.ToString()+"Start";
            label1.Text += "\n" + end.ToString()+"End";
            label1.Text += "\n" +range.ToString()+"Range";
            label1.Text += "\n" + rocky.getRange().ToString() + " Walk Range";
            label1.Text += "\n" + rocky.getRange().ToString() + " Walk Range";
            label1.Text += "\n" + new Point((range.X) / (rocky.getRange() * 3), range.Y / (rocky.getRange() * 3)).ToString() + " Walk step";
            */
            //timing
            if (m1_CurrentFrame >= rocky.getStart() && m1_CurrentFrame < rocky.getEnd())
            {
                if (counter%3==0)
                {
                    m1_CurrentFrame++;

                
                }
                counter++;
                rocky.shiftPoint((range.X) / (rocky.getRange()*3), range.Y / (rocky.getRange()*3));
            }
            else if (move)
            {
                m1_CurrentFrame = rocky.getStart();
                move = false;
            }

            else {
                previousRoom = mapData.GetPlayerPosition().RoomNumber;
                counter = 0;
            }

            if (m2_CurrentFrame >= rocky2.getStart() && m2_CurrentFrame < rocky2.getEnd())
            {
                m2_CurrentFrame++;
            }
            else
            {
                m2_CurrentFrame = 0;
            }
            //
            Rectangle rocky2Rect = new Rectangle(rocky2.getPoint(), rocky2.getSize());
            Rectangle rockyRect = new Rectangle(rocky.getPoint(), rocky.getSize());
            Rectangle rocky2PrevRect = new Rectangle(rocky2.getPastPoint(), rocky2.getSize());
            Rectangle rockyPrevRect = new Rectangle(rocky.getPastPoint(), rocky.getSize());
            using (Graphics g = Graphics.FromImage(buffer))
            {
                g.DrawImage(background, rocky2PrevRect, rocky2PrevRect, GraphicsUnit.Pixel);
                g.DrawImage(background, rockyPrevRect, rockyPrevRect, GraphicsUnit.Pixel);

                rocky2.renderTile(g, m2_CurrentFrame, rocky2Rect);
                rocky.renderTile(g, m1_CurrentFrame, rockyRect);

                g.DrawImage(foreground, rockyRect, rockyRect, GraphicsUnit.Pixel);
                g.DrawImage(foreground, rocky2Rect, rocky2Rect, GraphicsUnit.Pixel);
                g.DrawImage(foreground, rockyPrevRect, rockyPrevRect, GraphicsUnit.Pixel);
                g.DrawImage(foreground, rocky2PrevRect, rocky2PrevRect, GraphicsUnit.Pixel);
            }

            pnlDisplay.Invalidate(rockyRect);
            pnlDisplay.Invalidate(rockyPrevRect);
            pnlDisplay.Invalidate(rocky2Rect);
            pnlDisplay.Invalidate(rocky2PrevRect);
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(buffer, e.ClipRectangle, e.ClipRectangle, GraphicsUnit.Pixel);
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            label2.Text="None";
            if (counter == 0)
            {
                label2.Text ="Input";
                start = new Point((dispWidth / 7) * mapData.GetPlayerPosition().XCoordinate, (dispHeight / 7) * mapData.GetPlayerPosition().YCoordinate);
                if (e.KeyCode == Keys.Up)
                {
                    label2.Text += "Up";
                    mapData.MoveDown(); //up and down are flipped to account for the pixel location and 
                    rocky.setRange(10, 14);
                    move = true;
                }
                else if (e.KeyCode == Keys.Down)
                {
                    label2.Text += "Down";
                    mapData.CleanUpSpace(new MapPosition(mapData.GetPlayerPosition().RoomNumber, (mapData.GetPlayerPosition().XCoordinate), mapData.GetPlayerPosition().YCoordinate-1));
                    mapData.MoveUp();
                    rocky.setRange(0, 4);
                    move = true;
                }
                else if (e.KeyCode == Keys.Left)
                {
                    mapData.CleanUpSpace(new MapPosition(mapData.GetPlayerPosition().RoomNumber, (mapData.GetPlayerPosition().XCoordinate)-1, mapData.GetPlayerPosition().YCoordinate));
                    label2.Text += "Left";
                    mapData.MoveLeft();
                    //if(PlayerMoveResult == 3)
                    //{

                    //}
                    rocky.setRange(5, 9);
                        move = true;
                }
                else if (e.KeyCode == Keys.Right)
                {
                    label2.Text += "Right";
                    mapData.CleanUpSpace(new MapPosition(mapData.GetPlayerPosition().RoomNumber, mapData.GetPlayerPosition().XCoordinate+1, mapData.GetPlayerPosition().YCoordinate));
                    mapData.MoveRight();
                    rocky.setRange(15, 19);
                        move = true;
                }

                end=new Point(((dispWidth) / 7) * mapData.GetPlayerPosition().XCoordinate, ((dispHeight) / 7) * (mapData.GetPlayerPosition().YCoordinate));
                range = new Point(end.X-start.X,end.Y-start.Y) ;
                counter++;
            }

            e.Handled = true;
        }


        private void pnlButtons_Paint(object sender, PaintEventArgs e)
        {
             
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            BattleForm form = new BattleForm(inventory.players[0].level, inventory.players[1].level, inventory.players[2].level, 1 );
            timer1.Enabled = false;
            form.ShowDialog();
            timer1.Enabled = true;
        }

    }
}
