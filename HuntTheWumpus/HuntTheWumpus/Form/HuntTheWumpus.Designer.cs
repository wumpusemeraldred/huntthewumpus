﻿namespace HuntTheWumpus
{
    partial class HuntTheWumpus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.StartGameBtn = new System.Windows.Forms.Button();
            this.Background = new System.Windows.Forms.PictureBox();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.trivButton = new System.Windows.Forms.Button();
            this.best3of5 = new System.Windows.Forms.Button();
            this.best2of3 = new System.Windows.Forms.Button();
            this.HighScoreName = new System.Windows.Forms.Label();
            this.HighScoreTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Background)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // StartGameBtn
            // 
            this.StartGameBtn.Location = new System.Drawing.Point(78, 27);
            this.StartGameBtn.Name = "StartGameBtn";
            this.StartGameBtn.Size = new System.Drawing.Size(128, 51);
            this.StartGameBtn.TabIndex = 3;
            this.StartGameBtn.Text = "Start Game";
            this.StartGameBtn.UseVisualStyleBackColor = true;
            this.StartGameBtn.Click += new System.EventHandler(this.button2_Click);
            // 
            // Background
            // 
            this.Background.Location = new System.Drawing.Point(-1, -2);
            this.Background.Name = "Background";
            this.Background.Size = new System.Drawing.Size(291, 265);
            this.Background.TabIndex = 4;
            this.Background.TabStop = false;
            this.Background.Click += new System.EventHandler(this.Background_Click);
            // 
            // bindingSource1
            // 
            this.bindingSource1.CurrentChanged += new System.EventHandler(this.bindingSource1_CurrentChanged);
            // 
            // trivButton
            // 
            this.trivButton.Location = new System.Drawing.Point(105, 226);
            this.trivButton.Name = "trivButton";
            this.trivButton.Size = new System.Drawing.Size(75, 23);
            this.trivButton.TabIndex = 6;
            this.trivButton.Text = "Trivia";
            this.trivButton.UseVisualStyleBackColor = true;
            this.trivButton.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // best3of5
            // 
            this.best3of5.Location = new System.Drawing.Point(12, 226);
            this.best3of5.Name = "best3of5";
            this.best3of5.Size = new System.Drawing.Size(75, 23);
            this.best3of5.TabIndex = 7;
            this.best3of5.Text = "3of5";
            this.best3of5.UseVisualStyleBackColor = true;
            this.best3of5.Click += new System.EventHandler(this.button4_Click);
            // 
            // best2of3
            // 
            this.best2of3.Location = new System.Drawing.Point(197, 226);
            this.best2of3.Name = "best2of3";
            this.best2of3.Size = new System.Drawing.Size(75, 23);
            this.best2of3.TabIndex = 8;
            this.best2of3.Text = "2of3";
            this.best2of3.UseVisualStyleBackColor = true;
            this.best2of3.Click += new System.EventHandler(this.best2of3_Click);
            // 
            // HighScoreName
            // 
            this.HighScoreName.AutoSize = true;
            this.HighScoreName.Location = new System.Drawing.Point(119, 145);
            this.HighScoreName.Name = "HighScoreName";
            this.HighScoreName.Size = new System.Drawing.Size(38, 13);
            this.HighScoreName.TabIndex = 10;
            this.HighScoreName.Text = "Name:";
            // 
            // HighScoreTextBox
            // 
            this.HighScoreTextBox.Location = new System.Drawing.Point(78, 177);
            this.HighScoreTextBox.Name = "HighScoreTextBox";
            this.HighScoreTextBox.Size = new System.Drawing.Size(128, 20);
            this.HighScoreTextBox.TabIndex = 11;
            this.HighScoreTextBox.TextChanged += new System.EventHandler(this.HighScoreTextBox_TextChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.Control;
            this.button1.Location = new System.Drawing.Point(105, 103);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "Test Map";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // HuntTheWumpus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.HighScoreTextBox);
            this.Controls.Add(this.HighScoreName);
            this.Controls.Add(this.best2of3);
            this.Controls.Add(this.best3of5);
            this.Controls.Add(this.trivButton);
            this.Controls.Add(this.StartGameBtn);
            this.Controls.Add(this.Background);
            this.Name = "HuntTheWumpus";
            this.Text = "HuntTheWumpus";
            this.Load += new System.EventHandler(this.HuntTheWumpus_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Background)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button StartGameBtn;
        private System.Windows.Forms.PictureBox Background;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.Button trivButton;
        private System.Windows.Forms.Button best3of5;
        private System.Windows.Forms.Button best2of3;
        private System.Windows.Forms.Label HighScoreName;
        private System.Windows.Forms.TextBox HighScoreTextBox;
        private System.Windows.Forms.Button button1;
    }
}

