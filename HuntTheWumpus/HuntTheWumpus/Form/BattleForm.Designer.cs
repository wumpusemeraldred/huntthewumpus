﻿namespace HuntTheWumpus
{
    partial class BattleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.directorySearcher1 = new System.DirectoryServices.DirectorySearcher();
            this.Selection1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlDisplay = new System.Windows.Forms.Panel();
            this.hpLabel = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.Enemy1HP = new System.Windows.Forms.Label();
            this.Enemy2HP = new System.Windows.Forms.Label();
            this.Enemy3HP = new System.Windows.Forms.Label();
            this.pnlDisplay.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // directorySearcher1
            // 
            this.directorySearcher1.ClientTimeout = System.TimeSpan.Parse("-00:00:01");
            this.directorySearcher1.ServerPageTimeLimit = System.TimeSpan.Parse("-00:00:01");
            this.directorySearcher1.ServerTimeLimit = System.TimeSpan.Parse("-00:00:01");
            // 
            // Selection1
            // 
            this.Selection1.AutoSize = true;
            this.Selection1.Location = new System.Drawing.Point(18, 778);
            this.Selection1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Selection1.Name = "Selection1";
            this.Selection1.Size = new System.Drawing.Size(51, 20);
            this.Selection1.TabIndex = 0;
            this.Selection1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.label2.Location = new System.Drawing.Point(4, 938);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Command";
            // 
            // pnlDisplay
            // 
            this.pnlDisplay.Controls.Add(this.Enemy3HP);
            this.pnlDisplay.Controls.Add(this.Enemy2HP);
            this.pnlDisplay.Controls.Add(this.Enemy1HP);
            this.pnlDisplay.Controls.Add(this.hpLabel);
            this.pnlDisplay.Controls.Add(this.label2);
            this.pnlDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDisplay.Location = new System.Drawing.Point(0, 0);
            this.pnlDisplay.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pnlDisplay.Name = "pnlDisplay";
            this.pnlDisplay.Size = new System.Drawing.Size(1431, 1034);
            this.pnlDisplay.TabIndex = 5;
            this.pnlDisplay.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlDisplay_Paint);
            // 
            // hpLabel
            // 
            this.hpLabel.AutoSize = true;
            this.hpLabel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.hpLabel.Location = new System.Drawing.Point(189, 559);
            this.hpLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.hpLabel.Name = "hpLabel";
            this.hpLabel.Size = new System.Drawing.Size(51, 20);
            this.hpLabel.TabIndex = 6;
            this.hpLabel.Text = "label1";
            this.hpLabel.Click += new System.EventHandler(this.hpLabel_Click);
            // 
            // timer2
            // 
            this.timer2.Interval = 2000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // Enemy1HP
            // 
            this.Enemy1HP.AutoSize = true;
            this.Enemy1HP.Location = new System.Drawing.Point(958, 87);
            this.Enemy1HP.Name = "Enemy1HP";
            this.Enemy1HP.Size = new System.Drawing.Size(51, 20);
            this.Enemy1HP.TabIndex = 7;
            this.Enemy1HP.Text = "label1";
            // 
            // Enemy2HP
            // 
            this.Enemy2HP.AutoSize = true;
            this.Enemy2HP.Location = new System.Drawing.Point(958, 127);
            this.Enemy2HP.Name = "Enemy2HP";
            this.Enemy2HP.Size = new System.Drawing.Size(51, 20);
            this.Enemy2HP.TabIndex = 8;
            this.Enemy2HP.Text = "label1";
            // 
            // Enemy3HP
            // 
            this.Enemy3HP.AutoSize = true;
            this.Enemy3HP.Location = new System.Drawing.Point(958, 176);
            this.Enemy3HP.Name = "Enemy3HP";
            this.Enemy3HP.Size = new System.Drawing.Size(51, 20);
            this.Enemy3HP.TabIndex = 9;
            this.Enemy3HP.Text = "label1";
            // 
            // BattleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1431, 1034);
            this.Controls.Add(this.Selection1);
            this.Controls.Add(this.pnlDisplay);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "BattleForm";
            this.Text = "BattleForm";
            this.Load += new System.EventHandler(this.BattleForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key);
            this.pnlDisplay.ResumeLayout(false);
            this.pnlDisplay.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        private System.DirectoryServices.DirectorySearcher directorySearcher1;
        private System.Windows.Forms.Label Selection1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnlDisplay;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label hpLabel;
        private System.Windows.Forms.Label Enemy1HP;
        private System.Windows.Forms.Label Enemy2HP;
        private System.Windows.Forms.Label Enemy3HP;
    }
}