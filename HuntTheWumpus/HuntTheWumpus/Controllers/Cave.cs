﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace HuntTheWumpus
{
    class Cave
    {
        String[] linesEasy = File.ReadAllLines("Data\\WumpusCaveEasy.txt");
        // As a reminder:
        // int north = 0;
        // int northeast = 1;
        // int southeast = 2;
        // int south = 3;
        // int southwest = 4;
        // int northwest = 5;

        public Cave()
        {
            // toString with Cave.roomNumber?
        }


       // public void setDifficulty(int difficulty)
       // {
            // reads a different notepad cave file with more/less rooms and pathways depending on difficulty level.
            // will complete when all cave difficulties are fleshed out
       // }

        public void roomNumber()
        {
            // returns the room number the player is in
        }

        public int[] getAvailableRooms(int roomNumber) // DONE
        {
            String[] stringVersion = linesEasy[roomNumber - 1].Split(',');
            int[] intVersion = new int[6];
            for(int i = 0; i < 6; i++)
            {
                intVersion[i] = Int32.Parse(stringVersion[i]);
            }
            return intVersion;

            // n being roomNumber, we read line n of the text file
            // we then run a for loop 6 times, one for each of the exit directions
            // if the read integer is > 0, then the player can move to that room
            // UI reads the array of integers and parses it into a string with fancy format/grammar
        }

        public void move(int roomNumber, int direction)
        {
            // using roomNumber as the line of code accessed and direction as the index accessed
            // (direction being a number from 0-5)
            // if the return is positive we move the player to room caveFile(roomNumber, direction).
            // if it's negative, we return "You can't move there"
        }
    }
}