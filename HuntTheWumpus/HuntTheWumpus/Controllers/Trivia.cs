﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics; 

namespace HuntTheWumpus
{
    // The Question object simply stores the question, possible answers, and corect answer as strings.
    public class Question
    {
        public String question;
        public String answerA;
        public String answerB;
        public String answerC;
        public String answerD;
        public String correctAnswer;
    }

    public class Trivia
    {
        private Random gen = new Random();
        private List<Question> m_questions; // Stores all questions read from the file to a list of Questions
        private int m_qnum; // Number of the question to be asked
        private String m_ans; // Assigned to the answer input by the player
        private bool[] asked; // Keeps track of which questions have already been asked

        // The constructor is only called once at the beginning of the game.
        public Trivia()
        {
            initialize();
        }
        public void initialize()
        {
            // Find the file to be read and creates a StreamReader to read from that file
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            string path = Path.Combine(projectPath, @"Data", @"TriviaQuestions.txt");
            StreamReader sr = new StreamReader(path);
            m_questions = new List<Question>();
            // Reads the file line by line and stores in a Question object. Adds all questions to a list.
            while (!sr.EndOfStream)
            {
                Question q = new Question();
                q.question = sr.ReadLine();
                q.answerA = sr.ReadLine();
                q.answerB = sr.ReadLine();
                q.answerC = sr.ReadLine();
                q.answerD = sr.ReadLine();
                q.correctAnswer = sr.ReadLine();
                m_questions.Add(q);
                sr.ReadLine(); //there is an extra readline because in the text file containing the questions, an extra space was added for clarity
            }
            asked = new bool[m_questions.Count()]; //keeps track of which questions have been asked. True = asked, false = not asked
            m_qnum = gen.Next(0, m_questions.Count()); //randomly generated number of question to be asked
        }
        
        public bool CheckArray(bool[] asked)
        {
            //checks if each bool in the array is false, adds to a counter
            int counter = 0;
            for(int i = 0; i < asked.Count(); i++)
            {
                if (!asked[i])
                {
                    counter++;
                }
            }
            //if counter is greater than 0 then there is at least 1 false bool. (At least 1 question has not been asked yet)
            if(counter > 0)
            {
                return false;
            }
                return true;
        }
        public Question PickQuestion()
        {
            //if there are any questions that have not been asked, random question numbers are generated until it reaches a valid question
            if (!CheckArray(asked))
            {
                if (asked[m_qnum])
                {
                    m_qnum = gen.Next(0, m_questions.Count());
                    return PickQuestion();
                }
                else
                {
                    asked[m_qnum] = true;
                    return m_questions[m_qnum];
                }
            }
            //if the array is all true, the array is reset and pickquestion is called recursively
            else
            {
                for(int i = 0; i < asked.Count(); i++)
                {
                    asked[i] = false;
                }
                return PickQuestion();
            }
        }
        //GetAnswer returns the correct answer for the question that was asked as a string
        public String GetAnswer()
        {
            return m_questions[m_qnum].correctAnswer;
        }
        //SetUsersAnswer takes the answer input by the user
        public void SetUsersAnswer(String ans)
        {
            m_ans = ans;
        }
        //CheckAnswer returns whether the user got the question right or wrong.
        public bool CheckAnswer()
        {                                                                                                                                                                                                                                                                                                                                          
            if (GetAnswer().Equals(m_ans))
            {
                return true;
            }
            return false;
        }
        //Creates a form to ask the question and returns whether they got it right or wrong
        public bool AskQuestion(Trivia triv)
        {
            TriviaForm triviaForm = new TriviaForm(triv);
            triviaForm.ShowDialog();
            return CheckAnswer();
        }
        //repeatedly asks questions until 5 were asked or 3 were answered correctly. Returns whether they got 3 right
        public bool threeOfFive(Trivia triv)
        {
            int correct = 0;
            int incorrect = 0;
            while (correct < 3 && correct + incorrect < 5)
            {
                if (AskQuestion(triv))
                {
                    correct++;
                }
                else
                {
                    incorrect++;
                }
            }
            return correct >= 3;
        }
        //same as threeOfFive but with 2 out of 3
        public bool twoOfThree(Trivia triv)
        {
            int correct = 0;
            int incorrect = 0;
            while (correct < 2 && correct + incorrect < 3)
            {
                if (AskQuestion(triv))
                {
                    correct++;
                }
                else
                {
                    incorrect++;
                }
            }
            return correct >= 2;
        }

        //same as threeOfFive but accepts parameters for the minimum needed to be correct and the total to ask
        //For example, to play a game where you have to get 4 out of 5 right, call xOfY(trivia, 4, 5)
        public bool xOfY(Trivia triv, int x, int y)
        {
            int correct = 0;
            int incorrect = 0;
            while (correct < x && correct + incorrect < y)
            {
                if (AskQuestion(triv))
                {
                    correct++;
                }
                else
                {
                    incorrect++;
                }
            }
            return correct >= x;
        }
    }
}