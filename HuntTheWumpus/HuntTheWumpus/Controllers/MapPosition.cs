﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntTheWumpus.Controllers
{
    public class MapPosition
    {
        private int roomNumber;

        private int xCoordinate;

        private int yCoordinate;

        public int RoomNumber
        {
            get
            {
                return roomNumber;
            }

            set
            {
                roomNumber = value;
            }
        }

        public int XCoordinate
        {
            get
            {
                return xCoordinate;
            }

            set
            {
                xCoordinate = value;
            }
        }

        public int YCoordinate
        {
            get
            {
                return yCoordinate;
            }

            set
            {
                yCoordinate = value;
            }
        }

        public MapPosition()
        {
            roomNumber = 0;
            xCoordinate = 0;
            yCoordinate = 0;
        }

        public MapPosition(int inRoomNumber, int inXCoordinate, int inYCoordinate)
        {
            roomNumber = inRoomNumber;
            xCoordinate = inXCoordinate;
            yCoordinate = inYCoordinate;
        }

    }
}
