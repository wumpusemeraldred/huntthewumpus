﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// Author Rene Yu
namespace HuntTheWumpus
{
    class GameControl
    {
        private HighScore _HighScore;
        private Map _Map;
        //private Trivia _Trivia;
        //private UI _UI;
        private int difficulty;
        private String playername;


        public GameControl()
        {
            this._HighScore = new HighScore(playername);
            this._Map = new Map();
            //this._Trivia = new Trivia();
            //this._UI = new UI();
            
            
        }

        public void setPlayerName(String playername)
        {
            this.playername = playername;
        }

        public void setDifficulty(int difficulty)
        {
            this.difficulty = difficulty;
        }

        public void startGame(int difficulty, String playerName)
        {
            //Assigns corresponding Cave to game, updates UI with the Cave, places player in random starting room
        }

        public Boolean moveAvailable(int nextRoom, int currentRoom)
        {
            //Checks if moving to the selected room is valid, returns true if it is and vice versa
            return true;
        }

        public void enterNewRoom(int newRoom)
        {
            //Updates UI for the new room, coins, etc.
        }

        public int endGame(int numberOfMoves, int rawScore)
        {
            //Updates UI for victory/defeat, calculates send the final score to HighScore
            return 0;
        }
    }
}
