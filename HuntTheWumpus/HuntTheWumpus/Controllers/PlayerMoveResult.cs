﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntTheWumpus.Controllers
{
    public enum PlayerMoveResult
    {
        NotAdjacentSpace = 0,
        RoomTooFar,
        SpaceBlockedByWumpus,
        SpaceBlockedByTreasure,
        SpaceBlockedByObstacle,
        SpaceBlockedByWall,
        SpaceBlockedByEnemy,
        BadCoordinates,
        BadRoom,
        Success
    }
}
