﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntTheWumpus.Controllers
{
    public class Room
    {
        // each cell in the room can have exactly one of the following
        // empty space, blocked space/obstacle, player, bat, treasure, coin, wumpus
        // assume the player can exit the room from any cell
        // assume the bat never leaves the room
        // wumpus has a fixed location
        // on every player move, the bat moves as well as long as there is space
        public CellContent[,] cellContents;

        private int roomNumber;

        public int RoomNumber
        {
            get
            {
                return roomNumber;
            }

            set
            {
                roomNumber = value;
            }
        }
        public Room()
        {
            cellContents = new CellContent[5, 5];
            roomNumber = 0;
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    cellContents[i, j] = new AvailableSpace();
                }
            }
        }

        public Room(int roomNumberIn)
        {
            cellContents = new CellContent[5, 5];
            roomNumber = roomNumberIn;
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    cellContents[i, j] = new AvailableSpace();
                }
            }
        }

        public Room(int roomNumberIn, CellContent[,] cellContentIn)
        {
            cellContents = new CellContent[5, 5];
            roomNumber = roomNumberIn;
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    cellContents[i, j] = cellContentIn[i,j];
                }
            }
        }

        public MapPosition GetBatPosition()
        {
            MapPosition batPosition = new MapPosition();
            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                {
                    if (this.cellContents[i, j].GetType() == typeof(Bat))
                        batPosition = new MapPosition(this.roomNumber, i, j);
                }

            return batPosition;
        }

        public void MoveBat()
        {
            MapPosition batPosition = this.GetBatPosition();
            
            // the bat tries to move in this order
            // right, left, up, down
            // if none are possible, the bat stays in place
            // we could make the direction random, but it's not necessary
            if (batPosition.YCoordinate <= 3 &&
                !IsSpaceBlocked(batPosition.XCoordinate, batPosition.YCoordinate+1))
            {
                this.cellContents[batPosition.XCoordinate, batPosition.YCoordinate + 1] = this.cellContents[batPosition.XCoordinate, batPosition.YCoordinate];
                this.cellContents[batPosition.XCoordinate, batPosition.YCoordinate] = new AvailableSpace();
            }
            else if (batPosition.YCoordinate > 0 &&
                    !IsSpaceBlocked(batPosition.XCoordinate, batPosition.YCoordinate - 1))
            {
                this.cellContents[batPosition.XCoordinate, batPosition.YCoordinate - 1] = this.cellContents[batPosition.XCoordinate, batPosition.YCoordinate];
                this.cellContents[batPosition.XCoordinate, batPosition.YCoordinate] = new AvailableSpace();
            }
            else if (batPosition.XCoordinate <= 3 &&
                    !IsSpaceBlocked(batPosition.XCoordinate + 1, batPosition.YCoordinate))
            {
                this.cellContents[batPosition.XCoordinate + 1, batPosition.YCoordinate] = this.cellContents[batPosition.XCoordinate, batPosition.YCoordinate];
                this.cellContents[batPosition.XCoordinate, batPosition.YCoordinate] = new AvailableSpace();
            }
            else if (batPosition.XCoordinate > 0 &&
                    !IsSpaceBlocked(batPosition.XCoordinate - 1, batPosition.YCoordinate))
            {
                this.cellContents[batPosition.XCoordinate - 1, batPosition.YCoordinate] = this.cellContents[batPosition.XCoordinate, batPosition.YCoordinate];
                this.cellContents[batPosition.XCoordinate, batPosition.YCoordinate] = new AvailableSpace();
            }
        }

        public bool IsSpaceBlocked(int i, int j)
        {
            bool result = false;

            if (this.cellContents[i, j].GetType() == typeof(Wumpus) ||
                this.cellContents[i, j].GetType() == typeof(Coin) ||
                this.cellContents[i, j].GetType() == typeof(Obstacle) ||
                this.cellContents[i, j].GetType() == typeof(Treasure))
            {
                result = true;
            }

            return result;
        }

    }
}
