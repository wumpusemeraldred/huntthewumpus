using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntTheWumpus.Controllers;

namespace HuntTheWumpus
{
    public class Map
    {
        private Room[] rooms;

        // the location of the walls
        // this is going to be a number made of a combination of two positions
        private List<Tuple<MapPosition, MapPosition>> walls;

        private MapPosition playerPosition;

        public Map()
        {
            rooms = new Room[25];

            for (int i = 0; i < 25; i++)
            {
                rooms[i] = new Room(i);
            }

            this.InitializeDefaultPlayerPosition();

            walls = new List<Tuple<MapPosition, MapPosition>>();
        }

        public List<Tuple<MapPosition, MapPosition>> GetWalls()
        {
            return this.walls;
        }

        public Room[] GetRooms()
        {
            return this.rooms;
        }

        private void InitializeDefaultPlayerPosition()
        {
            playerPosition = new MapPosition(5, 2, 2);

            this.rooms[5].cellContents[2, 2] = new PlayerContent();
        }

        public void InitializeDefaultWalls()
        {
            walls = new List<Tuple<MapPosition, MapPosition>>();

            // room 0 to room 5
            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(0, 0, 4),
                new MapPosition(5, 0, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(0, 1, 4),
                new MapPosition(5, 1, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(0, 2, 4),
                new MapPosition(5, 2, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(0, 3, 4),
                new MapPosition(5, 3, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(0, 4, 4),
                new MapPosition(5, 4, 0)));

            // room 1 to room 6
            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(1, 0, 4),
                new MapPosition(6, 0, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(1, 1, 4),
                new MapPosition(6, 1, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(1, 2, 4),
                new MapPosition(6, 2, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(1, 3, 4),
                new MapPosition(6, 3, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(1, 4, 4),
                new MapPosition(6, 4, 0)));

            // room 5 to room 10
            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(5, 0, 4),
                new MapPosition(10, 0, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(5, 1, 4),
                new MapPosition(10, 1, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(5, 2, 4),
                new MapPosition(10, 2, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(5, 3, 4),
                new MapPosition(10, 3, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(5, 4, 4),
                new MapPosition(10, 4, 0)));

            // room 6 to room 11
            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(6, 0, 4),
                new MapPosition(11, 0, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(6, 1, 4),
                new MapPosition(11, 1, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(6, 2, 4),
                new MapPosition(11, 2, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(6, 3, 4),
                new MapPosition(11, 3, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(6, 4, 4),
                new MapPosition(11, 4, 0)));

            // room 7 to 12
            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(7, 0, 4),
                new MapPosition(12, 0, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(7, 1, 4),
                new MapPosition(12, 1, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(7, 2, 4),
                new MapPosition(12, 2, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(7, 3, 4),
                new MapPosition(12, 3, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(7, 4, 4),
                new MapPosition(12, 4, 0)));

            // room 8 to 13
            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(8, 0, 4),
                new MapPosition(13, 0, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(8, 1, 4),
                new MapPosition(13, 1, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(8, 2, 4),
                new MapPosition(13, 2, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(8, 3, 4),
                new MapPosition(13, 3, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(8, 4, 4),
                new MapPosition(13, 4, 0)));

            // room 11 to 16
            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(11, 0, 4),
                new MapPosition(16, 0, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(11, 1, 4),
                new MapPosition(16, 1, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(11, 2, 4),
                new MapPosition(16, 2, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(11, 3, 4),
                new MapPosition(16, 3, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(11, 4, 4),
                new MapPosition(16, 4, 0)));

            // room 12 to 17
            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(12, 0, 4),
                new MapPosition(17, 0, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(12, 1, 4),
                new MapPosition(17, 1, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(12, 2, 4),
                new MapPosition(17, 2, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(12, 3, 4),
                new MapPosition(17, 3, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(12, 4, 4),
                new MapPosition(17, 4, 0)));

            // room 13 to 18
            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(13, 0, 4),
                new MapPosition(18, 0, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(13, 1, 4),
                new MapPosition(18, 1, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(13, 2, 4),
                new MapPosition(18, 2, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(13, 3, 4),
                new MapPosition(18, 3, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(13, 4, 4),
                new MapPosition(18, 4, 0)));

            // room 14 to 19
            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(14, 0, 4),
                new MapPosition(19, 0, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(14, 1, 4),
                new MapPosition(19, 1, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(14, 2, 4),
                new MapPosition(19, 2, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(14, 3, 4),
                new MapPosition(19, 3, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(14, 4, 4),
                new MapPosition(19, 4, 0)));

            // room 15 to 20
            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(15, 0, 4),
                new MapPosition(20, 0, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(15, 1, 4),
                new MapPosition(20, 1, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(15, 2, 4),
                new MapPosition(20, 2, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(15, 3, 4),
                new MapPosition(20, 3, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(15, 4, 4),
                new MapPosition(20, 4, 0)));

            // room 18 to 23
            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(18, 0, 4),
                new MapPosition(23, 0, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(18, 1, 4),
                new MapPosition(23, 1, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(18, 2, 4),
                new MapPosition(23, 2, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(18, 3, 4),
                new MapPosition(23, 3, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(18, 4, 4),
                new MapPosition(23, 4, 0)));

            // room 2 to room 3
            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(2, 4, 0),
                new MapPosition(3, 0, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(2, 4, 1),
                new MapPosition(3, 0, 1)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(2, 4, 2),
                new MapPosition(3, 0, 2)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(2, 4, 3),
                new MapPosition(3, 0, 3)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(2, 4, 4),
                new MapPosition(3, 0, 4)));

            // room 8 to room 9
            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(8, 4, 0),
                new MapPosition(9, 0, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(8, 4, 1),
                new MapPosition(9, 0, 1)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(8, 4, 2),
                new MapPosition(9, 0, 2)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(8, 4, 3),
                new MapPosition(9, 0, 3)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(8, 4, 4),
                new MapPosition(9, 0, 4)));

            // room 16 to 17
            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(16, 4, 0),
                new MapPosition(17, 0, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(16, 4, 1),
                new MapPosition(17, 0, 1)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(16, 4, 2),
                new MapPosition(17, 0, 2)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(16, 4, 3),
                new MapPosition(17, 0, 3)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(16, 4, 4),
                new MapPosition(17, 0, 4)));

            // room 22 to 23
            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(22, 4, 0),
                new MapPosition(23, 0, 0)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(22, 4, 1),
                new MapPosition(23, 0, 1)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(22, 4, 2),
                new MapPosition(23, 0, 2)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(22, 4, 3),
                new MapPosition(23, 0, 3)));

            walls.Add(new Tuple<MapPosition, MapPosition>(
                new MapPosition(22, 4, 4),
                new MapPosition(23, 0, 4)));
        }

        public void InitializeDefaultRooms()
        {
            rooms = new Room[25];

            for (int i = 0; i < 25; i++)
            {
                rooms[i] = new Room(i);
            }

            this.InitializeRoom0();
            this.InitializeRoom1();
            this.InitializeRoom2();
            this.InitializeRoom3();
            this.InitializeRoom4();

            this.InitializeRoom5();
            this.InitializeRoom6();
            this.InitializeRoom7();
            this.InitializeRoom8();
            this.InitializeRoom9();

            this.InitializeRoom10();
            this.InitializeRoom11();
            this.InitializeRoom12();
            this.InitializeRoom13();
            this.InitializeRoom14();

            this.InitializeRoom15();
            this.InitializeRoom16();
            this.InitializeRoom17();
            this.InitializeRoom18();
            this.InitializeRoom19();

            this.InitializeRoom20();
            this.InitializeRoom21();
            this.InitializeRoom22();
            this.InitializeRoom23();
            this.InitializeRoom24();
        }

        public void InitializeDefaultMap()
        {
            this.InitializeDefaultWalls();

            this.InitializeDefaultRooms();

            this.InitializeDefaultPlayerPosition();
        }

        public MapPosition GetPlayerPosition()
        {
            return playerPosition;
        }

        public int[] GetCurrentRoomWalls() //for rendering walls
        {
            switch (this.playerPosition.RoomNumber)
            {
                case 0:
                    return new int[3] { 0, 2, 3 };
                case 1:
                    return new int[2] { 0, 3};
                case 2:
                    return new int[2] { 1, 3};
                case 3:
                    return new int[2] { 2, 3 };
                case 4:
                    return new int[2] { 1, 3};
                case 5:
                    return new int[3] { 0, 2, 3};
                case 6:
                    return new int[2] { 0, 3};
                case 7:
                    return new int[1] { 0};
                case 8:
                    return new int[2] { 0, 1 };
                case 9:
                    return new int[2] { 1, 2 };
                case 10:
                    return new int[2] { 2, 3 };
                case 11:
                    return new int[2] { 0, 3 };
                case 12:
                    return new int[2] { 0, 3 };
                case 13:
                    return new int[2] { 0, 3 };
                case 14:
                    return new int[2] { 0, 1 };
                case 15:
                    return new int[2] { 0, 2 };
                case 16:
                    return new int[2] { 1, 3 };
                case 17:
                    return new int[2] { 2, 3 };
                case 18:
                    return new int[2] { 0, 3 };
                case 19:
                    return new int[2] { 1, 3 };
                case 20:
                    return new int[3] { 0, 2, 3 };
                case 21:
                    return new int[1] { 0 };
                case 22:
                    return new int[2] { 0, 1 };
                case 23:
                    return new int[3] { 0, 2, 3 };
                case 24:
                    return new int[2] { 0, 1 };
            }

            return new int[4] { 0, 1, 2, 3 };
        }

        public int[] GetCurrentRoomOpenWalls() //gets accesibility between rooms, 0 = up, 1 = right, 2 = left, 3 = down
        {
            switch (this.playerPosition.RoomNumber)
            {
                case 0:
                    return new int[1] { 1 };
                case 1:
                    return new int[2] { 1, 2 };
                case 2:
                    return new int[2] { 0, 2 };
                case 3:
                    return new int[2] { 0, 1 };
                case 4:
                    return new int[2] { 0, 2 };
                case 5:
                    return new int[1] { 1 };
                case 6:
                    return new int[2] { 1, 2 };
                case 7:
                    return new int[3] { 1, 2, 3 };
                case 8:
                    return new int[2] { 2, 3 };
                case 9:
                    return new int[2] { 0, 3 };
                case 10:
                    return new int[2] { 0, 1 };
                case 11:
                    return new int[2] { 1, 2 };
                case 12:
                    return new int[2] { 1, 2 };
                case 13:
                    return new int[2] { 1, 2 };
                case 14:
                    return new int[2] { 2, 3 };
                case 15:
                    return new int[2] { 1, 3 };
                case 16:
                    return new int[2] { 0, 2 };
                case 17:
                    return new int[2] { 0, 1 };
                case 18:
                    return new int[2] { 1, 2 };
                case 19:
                    return new int[2] { 0, 2 };
                case 20:
                    return new int[1] { 1 };
                case 21:
                    return new int[3] { 1, 2, 3 };
                case 22:
                    return new int[2] { 2, 3 };
                case 23:
                    return new int[1] { 1 };
                case 24:
                    return new int[2] { 2, 3 };
            }

            return new int[4] { 0, 1, 2, 3 };
        }

        // resets a particular space on the map to available
        // this can be used after a battle or other interaction
        // that needs to clean up the space and make it available
        public void CleanUpSpace(MapPosition mapPosition)
        {
            if (mapPosition.RoomNumber > 24)
            {
                return;
            }

            if (mapPosition.XCoordinate < 0 ||
                mapPosition.XCoordinate > 4 ||
                mapPosition.YCoordinate < 0 ||
                mapPosition.YCoordinate > 4)
            {
                return;
            }

            this.rooms[mapPosition.RoomNumber].cellContents[mapPosition.XCoordinate, mapPosition.YCoordinate] = new AvailableSpace();
        }

        public PlayerMoveResult SetPlayerPosition(MapPosition newPosition)
        {
            // if trying to set the current position again do nothing
            // return success
            if (newPosition.RoomNumber == this.playerPosition.RoomNumber &&
                newPosition.XCoordinate == this.playerPosition.XCoordinate &&
                newPosition.YCoordinate == this.playerPosition.YCoordinate)
            {
                return PlayerMoveResult.Success;
            }

            if (newPosition.RoomNumber > 24)
            {
                return PlayerMoveResult.BadRoom;
            }

            if (newPosition.XCoordinate < 0 ||
                newPosition.XCoordinate > 4 ||
                newPosition.YCoordinate < 0 ||
                newPosition.YCoordinate > 4)
            {
                return PlayerMoveResult.BadCoordinates;
            }

            // if the player wants to switch rooms, but a wall is blocking
            // the move, return that
            if (newPosition.RoomNumber != this.playerPosition.RoomNumber)
            {
                Tuple<MapPosition, MapPosition> positionSeparator;

                if (newPosition.RoomNumber < this.playerPosition.RoomNumber)
                {
                    positionSeparator = new Tuple<MapPosition, MapPosition>(newPosition, this.playerPosition);
                }
                else
                {
                    positionSeparator = new Tuple<MapPosition, MapPosition>(this.playerPosition, newPosition);
                }

                for (int i = 0; i < this.walls.Count; i++)
                {
                    Tuple<MapPosition, MapPosition> wall = this.walls.ElementAt(i);
                    if ( wall.Item1.RoomNumber == positionSeparator.Item1.RoomNumber &&
                        wall.Item1.XCoordinate == positionSeparator.Item1.XCoordinate &&
                        wall.Item1.YCoordinate == positionSeparator.Item1.YCoordinate &&
                        wall.Item2.RoomNumber == positionSeparator.Item2.RoomNumber &&
                        wall.Item2.XCoordinate == positionSeparator.Item2.XCoordinate &&
                        wall.Item2.YCoordinate == positionSeparator.Item2.YCoordinate)
                    {
                        return PlayerMoveResult.SpaceBlockedByWall;
                    }
                }
            }

            // if the player wants to move where Wumpus is, return that
            if (this.rooms[newPosition.RoomNumber].cellContents[newPosition.XCoordinate, newPosition.YCoordinate].GetType() == typeof(Wumpus))
            {
                return PlayerMoveResult.SpaceBlockedByWumpus;
            }

            // if the player wants to move where an enemy is, return that
            if (this.rooms[newPosition.RoomNumber].cellContents[newPosition.XCoordinate, newPosition.YCoordinate].GetType() == typeof(Enemy))
            {
                return PlayerMoveResult.SpaceBlockedByEnemy;
            }

            // if the player's new position is blocked by an obstacle, return that
            if (this.rooms[newPosition.RoomNumber].cellContents[newPosition.XCoordinate, newPosition.YCoordinate].GetType() == typeof(Obstacle))
            {
                return PlayerMoveResult.SpaceBlockedByObstacle;
            }

            // if the player's new position is blocked by a treasure, return that
            if (this.rooms[newPosition.RoomNumber].cellContents[newPosition.XCoordinate, newPosition.YCoordinate].GetType() == typeof(Treasure))
            {
                return PlayerMoveResult.SpaceBlockedByTreasure;
            }

            // what should happen if a player moves onto a space with a coin?

            // if the room we are asked to move to is not either the current room
            // or an adjacen room, return the right value
            if (playerPosition.RoomNumber == newPosition.RoomNumber)
            {
                // the player is trying to move within the same room
                // check that coordinates are adjacent
                // the player's xCoordinate can change by 1
                // OR the player's yCoordinate can change by 1
                // but not both (the player cannot move diagonally)

                // also, if we are here, we already verified the space isn't blocked
                if ((Math.Abs(playerPosition.XCoordinate - newPosition.XCoordinate) == 1 &&
                    playerPosition.YCoordinate == newPosition.YCoordinate) ||
                    (Math.Abs(playerPosition.YCoordinate - newPosition.YCoordinate) == 1 &&
                    playerPosition.XCoordinate == newPosition.XCoordinate))
                {
                    MovePlayerAndBat(newPosition);
                    return PlayerMoveResult.Success;
                }
                else
                {
                    return PlayerMoveResult.BadCoordinates;
                }
            }
            else  if (newPosition.RoomNumber == playerPosition.RoomNumber + 1)
            {
                // if we are attempting to move right one room
                // the YCoordinates should be the same
                // XCoordinates should be 4 (player), 0 (new position)

                if (playerPosition.YCoordinate == newPosition.YCoordinate &&
                    playerPosition.XCoordinate == 4 &&
                    newPosition.XCoordinate == 0)
                {
                    MovePlayerAndBat(newPosition);
                    return PlayerMoveResult.Success;
                }
                else
                {
                    return PlayerMoveResult.BadCoordinates;
                }
            }
            else if (newPosition.RoomNumber == playerPosition.RoomNumber - 1)
            {
                // if we are attempting to move left one room
                // the YCoordinates should be the same
                // XCoordinates should be 0 (player), 4 (new position)

                if (playerPosition.YCoordinate == newPosition.YCoordinate &&
                    playerPosition.XCoordinate == 0 &&
                    newPosition.XCoordinate == 4)
                {
                    MovePlayerAndBat(newPosition);
                    return PlayerMoveResult.Success;
                }
                else
                {
                    return PlayerMoveResult.BadCoordinates;
                }
            }
            else if (newPosition.RoomNumber == playerPosition.RoomNumber + 5)
            {
                // if we are attempting to move up one room
                // the XCoordinates should be the same
                // YCoordinates should be 4 (player), 0 (new position)

                if (playerPosition.XCoordinate == newPosition.XCoordinate &&
                    playerPosition.YCoordinate == 4 &&
                    newPosition.YCoordinate == 0)
                {
                    MovePlayerAndBat(newPosition);
                    return PlayerMoveResult.Success;
                }
                else
                {
                    return PlayerMoveResult.BadCoordinates;
                }
            }
            else if (newPosition.RoomNumber == playerPosition.RoomNumber -5)
            {
                // if we are attempting to move down one room
                // the XCoordinates should be the same
                // YCoordinates should be 0 (player), 4 (new position)

                if (playerPosition.XCoordinate == newPosition.XCoordinate &&
                    playerPosition.YCoordinate == 0 &&
                    newPosition.YCoordinate == 4)
                {
                    MovePlayerAndBat(newPosition);
                    return PlayerMoveResult.Success;
                }
                else
                {
                    return PlayerMoveResult.BadCoordinates;
                }
            }

            return PlayerMoveResult.BadRoom;
        }

        public PlayerMoveResult MoveLeft()
        {
            MapPosition newPosition = new MapPosition(
                this.playerPosition.RoomNumber,
                this.playerPosition.XCoordinate,
                this.playerPosition.YCoordinate);

            // if we are at the left edge we need to move rooms, too
            if (this.playerPosition.XCoordinate == 0)
            {
                if (this.playerPosition.RoomNumber % 5 == 0)
                {
                    return PlayerMoveResult.SpaceBlockedByWall;
                }

                newPosition.RoomNumber = playerPosition.RoomNumber - 1;
                newPosition.XCoordinate = 4;
            }
            else
            {
                newPosition.XCoordinate--;
            }

            return this.SetPlayerPosition(newPosition);
        }

        public PlayerMoveResult MoveRight()
        {
            MapPosition newPosition = new MapPosition(
                this.playerPosition.RoomNumber,
                this.playerPosition.XCoordinate,
                this.playerPosition.YCoordinate);

            // if we are at the left edge we need to move rooms, too
            if (this.playerPosition.XCoordinate == 4)
            {
                if (this.playerPosition.RoomNumber % 5 == 4)
                {
                    return PlayerMoveResult.SpaceBlockedByWall;
                }

                newPosition.RoomNumber = playerPosition.RoomNumber + 1;
                newPosition.XCoordinate = 0;
            }
            else
            {
                newPosition.XCoordinate++;
            }

            return this.SetPlayerPosition(newPosition);
        }

        public PlayerMoveResult MoveUp()
        {
            MapPosition newPosition = new MapPosition(
                this.playerPosition.RoomNumber,
                this.playerPosition.XCoordinate,
                this.playerPosition.YCoordinate);

            // if we are at the left edge we need to move rooms, too
            if (this.playerPosition.YCoordinate == 4)
            {
                if (this.playerPosition.RoomNumber >= 20)
                {
                    return PlayerMoveResult.SpaceBlockedByWall;
                }

                newPosition.RoomNumber = playerPosition.RoomNumber + 5;
                newPosition.YCoordinate = 0;
            }
            else
            {
                newPosition.YCoordinate++;
            }

            return this.SetPlayerPosition(newPosition);
        }

        public PlayerMoveResult MoveDown()
        {
            MapPosition newPosition = new MapPosition(
                this.playerPosition.RoomNumber,
                this.playerPosition.XCoordinate,
                this.playerPosition.YCoordinate);

            // if we are at the left edge we need to move rooms, too
            if (this.playerPosition.YCoordinate == 0)
            {
                if (this.playerPosition.RoomNumber < 5)
                {
                    return PlayerMoveResult.SpaceBlockedByWall;
                }

                newPosition.RoomNumber = playerPosition.RoomNumber - 5;
                newPosition.YCoordinate = 4;
            }
            else
            {
                newPosition.YCoordinate--;
            }

            return this.SetPlayerPosition(newPosition);
        }

        public void MovePlayerAndBat(MapPosition newPosition)
        {
            // move the player
            playerPosition.RoomNumber = newPosition.RoomNumber;
            playerPosition.XCoordinate = newPosition.XCoordinate;
            playerPosition.YCoordinate = newPosition.YCoordinate;

            // move the bat in the new room
            // the bat always moves if it can when the player moves
            this.rooms[playerPosition.RoomNumber].MoveBat();
        }

        public void InitializeRoom0()
        {
            this.rooms[0].cellContents[2, 4] = new Obstacle();
            this.rooms[0].cellContents[1, 3] = new Treasure();
            this.rooms[0].cellContents[2, 3] = new Obstacle();
            this.rooms[0].cellContents[1, 2] = new Treasure();
            this.rooms[0].cellContents[2, 2] = new Treasure();
            this.rooms[0].cellContents[3, 2] = new Enemy();
            this.rooms[0].cellContents[2, 1] = new Enemy();
            this.rooms[0].cellContents[1, 0] = new Enemy();
        }

        public void InitializeRoom1()
        {
            this.rooms[1].cellContents[2, 3] = new Enemy();
            this.rooms[1].cellContents[1, 2] = new Enemy();
            this.rooms[1].cellContents[2, 2] = new Treasure();
            this.rooms[1].cellContents[3, 2] = new Enemy();
            this.rooms[1].cellContents[2, 1] = new Obstacle();
            this.rooms[1].cellContents[2, 0] = new Obstacle();
        }

        public void InitializeRoom2()
        {
            this.rooms[2].cellContents[1, 4] = new Obstacle();
            this.rooms[2].cellContents[4, 4] = new Obstacle();
            this.rooms[2].cellContents[2, 3] = new Obstacle();
            this.rooms[2].cellContents[4, 3] = new Obstacle();
            this.rooms[2].cellContents[4, 2] = new Obstacle();
            this.rooms[2].cellContents[4, 1] = new Obstacle();
            this.rooms[2].cellContents[0, 0] = new Enemy();
            this.rooms[2].cellContents[1, 0] = new Enemy();
            this.rooms[2].cellContents[2, 0] = new Enemy();
            this.rooms[2].cellContents[3, 0] = new Enemy();
            this.rooms[2].cellContents[4, 0] = new Obstacle();
        }

        public void InitializeRoom3()
        {
            this.rooms[3].cellContents[2, 3] = new Enemy();
            this.rooms[3].cellContents[1, 2] = new Enemy();
            this.rooms[3].cellContents[3, 2] = new Enemy();
            this.rooms[3].cellContents[2, 1] = new Enemy();
        }

        public void InitializeRoom4()
        {
            this.rooms[4].cellContents[2, 4] = new Enemy();
            this.rooms[4].cellContents[3, 4] = new Enemy();
            this.rooms[4].cellContents[4, 4] = new Treasure();
            this.rooms[4].cellContents[0, 3] = new Obstacle();
            this.rooms[4].cellContents[3, 3] = new Enemy();
            this.rooms[4].cellContents[4, 3] = new Enemy();
            this.rooms[4].cellContents[0, 2] = new Obstacle();
            this.rooms[4].cellContents[4, 2] = new Enemy();
            this.rooms[4].cellContents[0, 1] = new Obstacle();
            this.rooms[4].cellContents[0, 0] = new Obstacle();
        }

        public void InitializeRoom5()
        {
        }

        public void InitializeRoom6()
        {
            this.rooms[6].cellContents[1, 4] = new Enemy();
            this.rooms[6].cellContents[4, 4] = new Enemy();
            this.rooms[6].cellContents[4, 3] = new Enemy();
            this.rooms[6].cellContents[4, 2] = new Obstacle();
            this.rooms[6].cellContents[4, 1] = new Obstacle();
            this.rooms[6].cellContents[4, 0] = new Obstacle();
        }

        public void InitializeRoom7()
        {
            this.rooms[7].cellContents[4, 4] = new Obstacle();
            this.rooms[7].cellContents[4, 3] = new Obstacle();
            this.rooms[7].cellContents[4, 2] = new Obstacle();
            this.rooms[7].cellContents[4, 1] = new Enemy();
            this.rooms[7].cellContents[4, 0] = new Enemy();
        }

        public void InitializeRoom8()
        {
            this.rooms[8].cellContents[0, 4] = new Obstacle();
            this.rooms[8].cellContents[1, 4] = new Obstacle();
            this.rooms[8].cellContents[3, 4] = new Obstacle();
            this.rooms[8].cellContents[4, 4] = new Obstacle();
            this.rooms[8].cellContents[4, 3] = new Obstacle();
            this.rooms[8].cellContents[2, 2] = new Obstacle();
            this.rooms[8].cellContents[1, 1] = new Obstacle();
        }

        public void InitializeRoom9()
        {
            this.rooms[9].cellContents[3, 4] = new Obstacle();
            this.rooms[9].cellContents[1, 2] = new Enemy();
            this.rooms[9].cellContents[2, 1] = new Enemy();
        }

        public void InitializeRoom10()
        {
            this.rooms[10].cellContents[2, 4] = new Enemy();
            this.rooms[10].cellContents[1, 2] = new Enemy();
        }

        public void InitializeRoom11()
        {
            this.rooms[11].cellContents[1, 4] = new Obstacle();
            this.rooms[11].cellContents[1, 3] = new Obstacle();
            this.rooms[11].cellContents[3, 3] = new Obstacle();
            this.rooms[11].cellContents[4, 2] = new Obstacle();
            this.rooms[11].cellContents[0, 1] = new Obstacle();
            this.rooms[11].cellContents[2, 1] = new Obstacle();
            this.rooms[11].cellContents[0, 0] = new Obstacle();
            this.rooms[11].cellContents[4, 0] = new Enemy();
        }

        public void InitializeRoom12()
        {
            this.rooms[12].cellContents[3, 4] = new Obstacle();
            this.rooms[12].cellContents[1, 3] = new Enemy();
            this.rooms[12].cellContents[3, 3] = new Obstacle();
            this.rooms[12].cellContents[1, 1] = new Obstacle();
            this.rooms[12].cellContents[2, 1] = new Obstacle();
        }

        public void InitializeRoom13()
        {
            this.rooms[13].cellContents[3, 4] = new Obstacle();
            this.rooms[13].cellContents[3, 3] = new Obstacle();
            this.rooms[13].cellContents[3, 2] = new Enemy();
            this.rooms[13].cellContents[2, 1] = new Enemy();
        }

        public void InitializeRoom14()
        {
            this.rooms[14].cellContents[1, 3] = new Obstacle();
            this.rooms[14].cellContents[3, 3] = new Obstacle();
            this.rooms[14].cellContents[0, 2] = new Enemy();
            this.rooms[14].cellContents[2, 2] = new Obstacle();
            this.rooms[14].cellContents[3, 1] = new Obstacle();
        }

        public void InitializeRoom15()
        {
            this.rooms[15].cellContents[2, 4] = new Obstacle();
            this.rooms[15].cellContents[3, 3] = new Obstacle();
            this.rooms[15].cellContents[3, 2] = new Obstacle();
            this.rooms[15].cellContents[2, 1] = new Obstacle();
            this.rooms[15].cellContents[3, 0] = new Enemy();
        }

        public void InitializeRoom16()
        {
            this.rooms[16].cellContents[1, 3] = new Obstacle();
            this.rooms[16].cellContents[2, 2] = new Obstacle();
            this.rooms[16].cellContents[3, 1] = new Obstacle();
            this.rooms[16].cellContents[1, 0] = new Obstacle();
        }

        public void InitializeRoom17()
        {
            this.rooms[17].cellContents[1, 4] = new Obstacle();
            this.rooms[17].cellContents[3, 3] = new Obstacle();
            this.rooms[17].cellContents[1, 2] = new Obstacle();
            this.rooms[17].cellContents[4, 2] = new Obstacle();
            this.rooms[17].cellContents[2, 1] = new Obstacle();
            this.rooms[17].cellContents[4, 1] = new Obstacle();
            this.rooms[17].cellContents[2, 0] = new Obstacle();
        }

        public void InitializeRoom18()
        {
            this.rooms[18].cellContents[1, 3] = new Enemy();
            this.rooms[18].cellContents[2, 3] = new Obstacle();
            this.rooms[18].cellContents[2, 2] = new Enemy();
            this.rooms[18].cellContents[3, 2] = new Obstacle();
            this.rooms[18].cellContents[1, 1] = new Enemy();
            this.rooms[18].cellContents[2, 1] = new Obstacle();
        }

        public void InitializeRoom19()
        {
            this.rooms[19].cellContents[3, 3] = new Obstacle();
            this.rooms[19].cellContents[2, 2] = new Enemy();
            this.rooms[19].cellContents[3, 2] = new Obstacle();
            this.rooms[19].cellContents[1, 1] = new Obstacle();
            this.rooms[19].cellContents[2, 1] = new Obstacle();
            this.rooms[19].cellContents[3, 1] = new Obstacle();
            this.rooms[19].cellContents[4, 1] = new Enemy();
        }

        public void InitializeRoom20()
        {
            this.rooms[20].cellContents[0, 4] = new Treasure();
            this.rooms[20].cellContents[1, 4] = new Treasure();
            this.rooms[20].cellContents[2, 4] = new Treasure();
            this.rooms[20].cellContents[3, 4] = new Treasure();
            this.rooms[20].cellContents[4, 4] = new Treasure();
            this.rooms[20].cellContents[0, 3] = new Treasure();
            this.rooms[20].cellContents[1, 3] = new Treasure();
            this.rooms[20].cellContents[3, 3] = new Enemy();
            this.rooms[20].cellContents[4, 3] = new Obstacle();
            this.rooms[20].cellContents[0, 2] = new Treasure();
            this.rooms[20].cellContents[2, 2] = new Enemy();
            this.rooms[20].cellContents[4, 2] = new Obstacle();
            this.rooms[20].cellContents[2, 1] = new Enemy();
            this.rooms[20].cellContents[3, 1] = new Enemy();
            this.rooms[20].cellContents[4, 1] = new Enemy();
            this.rooms[20].cellContents[1, 0] = new Enemy();
            this.rooms[20].cellContents[3, 0] = new Enemy();
            this.rooms[20].cellContents[4, 0] = new Obstacle();
        }

        public void InitializeRoom21()
        {
            this.rooms[21].cellContents[0, 4] = new Obstacle();
            this.rooms[21].cellContents[1, 4] = new Obstacle();
            this.rooms[21].cellContents[2, 4] = new Treasure();
            this.rooms[21].cellContents[3, 4] = new Treasure();
            this.rooms[21].cellContents[4, 4] = new Obstacle();
        }

        public void InitializeRoom22()
        {
            this.rooms[22].cellContents[0, 4] = new Obstacle();
            this.rooms[22].cellContents[2, 4] = new Treasure();
            this.rooms[22].cellContents[3, 4] = new Treasure();
            this.rooms[22].cellContents[4, 4] = new Treasure();
            this.rooms[22].cellContents[0, 3] = new Enemy();
            this.rooms[22].cellContents[3, 3] = new Treasure();
            this.rooms[22].cellContents[4, 3] = new Treasure();
            this.rooms[22].cellContents[0, 2] = new Enemy();
            this.rooms[22].cellContents[0, 1] = new Enemy();
        }

        public void InitializeRoom23()
        {
            this.rooms[23].cellContents[2, 2] = new Wumpus();
        }

        public void InitializeRoom24()
        {
            this.rooms[24].cellContents[3, 3] = new Obstacle();
            this.rooms[24].cellContents[2, 2] = new Obstacle();
            this.rooms[24].cellContents[1, 1] = new Obstacle();
            this.rooms[24].cellContents[0, 0] = new Obstacle();
            this.rooms[24].cellContents[1, 0] = new Treasure();
            this.rooms[24].cellContents[3, 0] = new Enemy();
        }
    }
}
    
