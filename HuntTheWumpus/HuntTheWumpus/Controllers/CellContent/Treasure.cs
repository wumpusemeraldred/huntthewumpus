﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntTheWumpus.Controllers
{
    public class Treasure : CellContent
    {
        private int numberOfCoins;

        private int numberOfArrows;

        private bool collected;

        public int NumberOfCoins
        {
            get
            {
                return numberOfCoins;
            }

            set
            {
                numberOfCoins = value;
            }
        }

        public int NumberOfArrows
        {
            get
            {
                return numberOfArrows;
            }

            set
            {
                numberOfArrows = value;
            }
        }

        public bool Collected
        {
            get
            {
                return collected;
            }

            set
            {
                collected = value;
            }
        }

        public Treasure()
        {
            numberOfArrows = 0;
            numberOfCoins = 0;
            collected = false;
        }

        public Treasure(int inNumberOfArrows, int inNumberOfCoin)
        {
            numberOfArrows = inNumberOfArrows;
            numberOfCoins = inNumberOfCoin;
            collected = false;
        }
    }
}
