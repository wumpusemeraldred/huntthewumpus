﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using HuntTheWumpus.Battle.Entities;


namespace HuntTheWumpus.Controllers.UI_Stuff
{
    class BattleMenu
    {
        private int[][][] _mainMenu;

        private int[][] _attack;

        private int[][] _skills;

        private int[][] _tactics;
        private int[] _scan;
        private int[] _equip;
        private int[] _switch;

        private int[][] _items;

        private int _selection;
        private int _subSelection;
        private int _selectionMax;
        private int _subSelectionMax;
        private int[] _menuOutput;
        private Boolean _filled;


        public BattleMenu(PlayerCharacter player)
        {
            _items = new int[4][];
            for (int i = 0; i < _items.Length; i++)
            {
                _items[i] = new int[] { 0, 1, 2, 3 };
            }
        }

        public BattleMenu()
        {
            _menuOutput = new int[] { 0, 0, 0, 0};

            _attack = new int[1][];
            _attack[0] = new int[] { 0, 1, 2, 3 };
            _skills = new int[4][];
            for (int i = 0; i < _skills.Length; i++)
            {
                _skills[i] = new int[] { 0, 1, 2, 3 };
            }

            _tactics = new int[3][];
            _scan = new int[] { 0, 1, 2, 3 };
            _equip = new int[] { 0, 1, 2, 3, 4 };
            _switch = new int[] { 0, 1, 2 };
            _tactics[0] = _scan;
            _tactics[1] = _equip;
            _tactics[2] = _switch;

            _items = new int[4][];
            for (int i = 0; i < _items.Length; i++)
            {
                _items[i] = new int[] { 0, 1, 2, 3 };
            }


            _mainMenu = new int[4][][];
            _mainMenu[0] = _attack;
            _mainMenu[1] = _skills;
            _mainMenu[2] = _tactics;
            _mainMenu[3] = _items;

            _selection = 0;
            _subSelection = 0;
            _selectionMax = 0;
            _subSelectionMax = 0;
            _filled = false;
    }
        public void shiftSelection(object sender, KeyEventArgs e)
        {
            //sets max
            if (_subSelection == 0)
            {
                _selectionMax = _mainMenu.Length-1;
            }
            else if (_subSelection == 1)
            {
                _selectionMax = _mainMenu[_menuOutput[0]].Length - 1;
            }
            else if (_subSelection == 2)
            {
                _selectionMax = _mainMenu[_menuOutput[0]][_menuOutput[1]].Length-1;
            }

                _subSelectionMax = 2;

            //Reads input
            if (e.KeyCode == Keys.Up && _selection > 0)
            {
                _selection--;
            }
            else if (e.KeyCode == Keys.Up)
            {
                _selection = _selectionMax;
            }

            if (e.KeyCode == Keys.Down && _selection < _selectionMax)
            {
                _selection++;
            }
            else if (e.KeyCode == Keys.Down)
            {
                _selection = 0;
            }

            if (e.KeyCode == Keys.Left && _menuOutput[0] == 0 && _subSelection > 0)
            {
                _subSelection-=2;
                _selection = _menuOutput[_subSelection];
            }
            else if (e.KeyCode == Keys.Left && _subSelection > 0)
            {
                _subSelection--;
                _selection = _menuOutput[_subSelection];
            }

            if (e.KeyCode == Keys.Right&& _menuOutput[0] == 0 && _subSelection < _subSelectionMax)
              {
                
                _subSelection+=2;
                _selection = 0;
            }
            else if (e.KeyCode == Keys.Right && _subSelection < _subSelectionMax)
            {
                _subSelection++;
                _selection = 0;
            }

            //Handles output
            _menuOutput[_subSelection] = _selection;

            if (_subSelection == 0)
            {
                _menuOutput[1] = 0;
                _menuOutput[2] = 0;
            }
            else if (_subSelection == 1)
            {
                _menuOutput[2] = 0;
            }
            if (_subSelection == 2)
            {
                _filled = true;
            }
            else
            {
                _filled = false;
            }
            e.Handled = true;
        }

        public void displayMenuOutput(Label menu)
        {
            menu.Text = "";
            for (int i = 0; i < _subSelection; i++)
            {
                menu.Text += _menuOutput[i].ToString()+" ";
            }
            menu.Text += _selection;
            menu.Text += "\n" +_selectionMax;
            menu.Text += "\n" +_filled.ToString();
            menu.Text += "\n" + _menuOutput.Length.ToString();
        }

        public int getSelectionOfSubSelection(int subSelection)
        {
            return _menuOutput[subSelection];
        }
        public int getSubSelection()
        {
            return _subSelection;
        }
        public int getMenuOutput(int i)
        {
            return _menuOutput[i];
        }
        public int[] getMenuOutput()
        {
            return _menuOutput;
        }
        public Boolean getFill()
        {
            return _filled;
        }
    }
}
