﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntTheWumpus.Controllers
{
    class SpriteTypes
    {
        private Sprite[,] _mapSprites;
        public SpriteTypes()
        {
        }
        public Sprite[,] mapSpriteConverter(int[,]array)
        {
           _mapSprites = new Sprite[array.GetLength(0), array.GetLength(1)];
            for (int a=0; a < array.GetLength(0);a++)
            {
                for (int b = 0; b < array.GetLength(1); b++)
                {
                    Point location = new Point(a*96+96,b*96+192);
                    int temp= array[a,b];
                    if (array[a, b] == 0)
                    {
                        _mapSprites[a, b] = null;
                    }
                    else if (array[a, b] == 1)
                    {
                        _mapSprites[a, b] = new Sprite(".\\Images\\RockSpreadSheet.png", 4, 128, 32, location);
                    }
                    else if (array[a, b] == 2)
                    {
                        _mapSprites[a, b] = _mapSprites[a, b] = new Sprite(".\\Images\\MapSprite\\RockType1.png",1 ,128, 32, location);
                    }
                    else
                    {
                        _mapSprites[a, b] = null;
                    }
                }
            }
            return _mapSprites;
        }


    }
}
