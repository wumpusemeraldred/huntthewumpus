﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HuntTheWumpus
{
    public class Sprite
    {
        
        private String _fileLocation;
        private Bitmap[] _animationTiles;
        private int _numberOfColumns;
        private int _tileWidth;
        private int _tileHeight;
        private int _sheetWidth;
        private int _sheetHeight;
        private int _step;
        private int _currentColumn = 0;
        private Point _location;
        private Point _previousLocation;
        private Point _range;

        public Sprite(string FileLocation, int NumberOfColumns, int PictureWidth, int PictureHeight, Point ? location = null)
        {
            //1 step constructors
            _fileLocation = FileLocation;
            _numberOfColumns = NumberOfColumns;

            Bitmap sheet = new Bitmap(_fileLocation);
        
            _sheetWidth = sheet.Width;
            _sheetHeight = sheet.Height;
            _location = location ?? new Point(0, 0);
            _previousLocation = new Point(128, 0);
            _range = new Point(0, NumberOfColumns-1);
            _tileHeight = _sheetHeight;

            //2 step constructors
            _step = PictureWidth / _numberOfColumns;
            _tileWidth = _sheetWidth / _numberOfColumns;

            //array constructors
            _animationTiles = new Bitmap[_numberOfColumns];

            

            Rectangle destRect = new Rectangle(0, 0, _tileWidth, _tileHeight);

            for (int c = 0; c < _numberOfColumns; c++)
            {
                int frameStart = c * _tileWidth;
                Rectangle srcRect = new Rectangle(frameStart, 0, _tileWidth, _tileHeight);
                Bitmap tile = new Bitmap(_tileWidth, _tileHeight);
                Graphics gc = Graphics.FromImage(tile);
                gc.DrawImage(sheet, destRect, srcRect, GraphicsUnit.Pixel);
                _animationTiles[c] = tile;
            }
        }

        //methods
        public string getFileLocation()
        {
            return _fileLocation;
        }
        public int getNumberOfColumns()
        {
            return _numberOfColumns;
        }
        public int getCurrentColumn()
        {
            return _currentColumn;
        }
        public int getStep() {
            return _step;
        }
        public Point getPoint()
        {
            return _location;
        }
        public Point getPastPoint()
        {
            return _previousLocation;
        }
        public Size getSize() {
            return new Size(_tileWidth,_tileHeight);
        }
        public void shiftPoint(int x, int y)
        {
            _previousLocation = _location;
            _location = new Point(_location.X + x, _location.Y + y);
        }
        public void setPoint(int x, int y)
        {
            _previousLocation = _location;
            _location = new Point(x,y);
        }
        public int getStart()
        {
            return _range.X;
        }
        public int getEnd()
        {
            return _range.Y;
        }
        public void setRange(int start , int end)
        {
            _range.X = start;
            _range.Y = end;
        }
        public void setRange(int frame)
        {
            _range.X = frame;
            _range.Y = frame;
        }
        public int getRange()
        {
            if (_range.X == _range.Y)
            {
                return 1;
            }
            else
            { 
            return _range.Y - _range.X;
            }
        }
        public void renderTile(PaintEventArgs e, int column, Rectangle toRect)
        {
            e.Graphics.DrawImage(_animationTiles[column], toRect);
        }
        public void renderTile(Graphics g, int column, Rectangle toRect)
        {
            g.DrawImage(_animationTiles[column], toRect);
        }
    }
}