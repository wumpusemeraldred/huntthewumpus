﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntTheWumpus.Controllers.UI_Stuff
{
    class RoomGraphics
    {
        private Bitmap[] paths;
        private Bitmap[] walls;
        private Bitmap[] rails;
        private Bitmap[] poles;
        private Bitmap[] obstaclesBit;
        private int dispWidth;
        private int dispHeight;
        public RoomGraphics()
        {
        }
        public RoomGraphics(int DispWidth, int DispHeight) {
            //0=Up,1=Right,2=Down,3=Left,none
            this.dispWidth = DispWidth;
            this.dispHeight = DispHeight;
            paths = new Bitmap[5];
            paths[0] = new Bitmap(".\\Images\\RoomConstructor\\Background\\Path\\CaveBackgroundTopPath.png");
            paths[1] = new Bitmap(".\\Images\\RoomConstructor\\Background\\Path\\CaveBackgroundRightPath.png");
            paths[2] = new Bitmap(".\\Images\\RoomConstructor\\Background\\Path\\CaveBackgroundLeftPath.png");
            paths[3] = new Bitmap(".\\Images\\RoomConstructor\\Background\\Path\\CaveBackgroundBottomPath.png");
            paths[4] = new Bitmap(".\\Images\\RoomConstructor\\Background\\Path\\CaveBackgroundFloor.png");

            walls = new Bitmap[4];
            walls[0] = new Bitmap(".\\Images\\RoomConstructor\\Background\\Walls\\CaveBackgroundTopWall.png");
            walls[1] = new Bitmap(".\\Images\\RoomConstructor\\Foreground\\Walls\\CaveBackgroundRightWall.png");
            walls[2] = new Bitmap(".\\Images\\RoomConstructor\\Foreground\\Walls\\CaveBackgroundLeftWall.png");
            walls[3] = new Bitmap(".\\Images\\RoomConstructor\\Foreground\\Walls\\CaveBackgroundBottomWall.png");

            rails = new Bitmap[4];
            rails[0] = new Bitmap(".\\Images\\RoomConstructor\\Foreground\\Rail\\CaveBackgroundTopRail.png");
            rails[1] = new Bitmap(".\\Images\\RoomConstructor\\Foreground\\Rail\\CaveBackgroundRightRail.png");
            rails[2] = new Bitmap(".\\Images\\RoomConstructor\\Foreground\\Rail\\CaveBackgroundLeftRail.png");
            rails[3] = new Bitmap(".\\Images\\RoomConstructor\\Foreground\\Rail\\CaveBackgroundBottomRail.png");

            //Up,Down
            poles = new Bitmap[2];
            poles[0] = new Bitmap(".\\Images\\RoomConstructor\\Background\\Poles\\CaveBackgroundTopPole.png");
            poles[1] = new Bitmap(".\\Images\\RoomConstructor\\Foreground\\Poles\\CaveBackgroundBottomPole.png");

            obstaclesBit = new Bitmap[3];
            obstaclesBit[1] = new Bitmap(".\\Images\\MapObstacleSprite\\RockTypes1.png");
            obstaclesBit[2] = new Bitmap(".\\Images\\MapObstacleSprite\\ChestSpriteSheet.png");




            /*obstacles = new Bitmap[2];
            obstacles[0]= new Bitmap("");
            obstacles[1] = new Bitmap("");
            obstacles[2] = new Bitmap("");
            obstacles[3] = new Bitmap("");
            obstacles[4] = new Bitmap("");*/
        }
        public void DrawForeground(Graphics g, int[] directions)
        {//changes the order for rendering
            int[] intArray=new int[] {0,1,2,3};
            Boolean BottomRail = false;
            foreach (int i in directions)
            {
                intArray[i] = -1;
            }
            for (int i = 0; i < 4; i++)
            {
                if (i==3&&intArray[i] != -1)
                {
                     g.DrawImage(walls[i], new Rectangle(0, 0, dispWidth, dispHeight));
                }
            }
            foreach (int i in directions)
            {
                if (i != 3)
                {
                    g.DrawImage(rails[i], new Rectangle(0, 0, dispWidth, dispHeight));
                }
            }
            foreach (int i in directions)
            {
                if (i == 3)
                {
                    g.DrawImage(new Bitmap(".\\Images\\RoomConstructor\\Foreground\\Walls\\CaveBackgroundBottomPathWalls.png"), new Rectangle(0, 0, dispWidth, dispHeight));
                    g.DrawImage(new Bitmap(".\\Images\\RoomConstructor\\Foreground\\Beam\\CaveBackgroundTopBeams.png"), new Rectangle(0, 0, dispWidth, dispHeight));
                    g.DrawImage(rails[i], new Rectangle(0, 0, dispWidth, dispHeight));
                    BottomRail = true;
                }
            }
            if (!BottomRail)
            {
                foreach (int i in directions)
                {
                    if (i == 1)
                    {
                        g.DrawImage(poles[i], new Rectangle(0, 0, dispWidth, dispHeight));
                    }
                }
                g.DrawImage(new Bitmap(".\\Images\\RoomConstructor\\Foreground\\Beam\\CaveBackgroundTopBeams.png"), new Rectangle(0, 0, dispWidth, dispHeight));
            }
        }

        public void DrawBackground(Graphics g, int[] directions)
        {
            int[] intArray = new int[] { 0, 1, 2, 3 };
            g.DrawImage(paths[4], new Rectangle(0, 0, dispWidth, dispHeight));
            foreach (int i in directions)
            {
                g.DrawImage(paths[i], new Rectangle(0, 0, dispWidth, dispHeight));
                intArray[i]= -1;
            }
            for(int i = 0; i < 4;i++)
            {
                if (intArray[i] != -1)
                {
                    g.DrawImage(walls[i], new Rectangle(0, 0, dispWidth, dispHeight));
                }
            }
            g.DrawImage(poles[0], new Rectangle(0, 0, dispWidth, dispHeight));
        }
        public void DrawObstacles(Graphics g, int[,] obstacles)
        {
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    if (obstacles[i,j]==1) //draw rocks
                    {
                        g.DrawImage(obstaclesBit[1], new Rectangle(dispWidth / 7*i, dispHeight / 7 * j, dispWidth, dispHeight));
                    }
                    else if (obstacles[i, j] == 2) //draw chests
                    {
                        g.DrawImage(obstaclesBit[2], new Rectangle(dispWidth / 7 * i, dispHeight / 7 * j, dispWidth, dispHeight));
                    }
                }
            }
        }
    }
}
