﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace HuntTheWumpus
{
    public class Player
    {
        public Player(string name, int score)//stores a string for the name and an int for the high score the player has recieved
        {
            Name = name;
            Score = score;
        }

        string Name;
        int Score;
    }

    public class HighScore
    {
        string PlayerName;
        string FilePath;

        int numHighscoresToDisplay = 10;//the number of scores that will be displayed on the Form

        public HighScore(String playerName)
        {
            PlayerName = playerName;
            string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            FilePath = Path.Combine(projectPath, @"Data", @"HighScore.txt");
            if (!File.Exists(FilePath))
            {
                FileStream fs = File.Create(FilePath);
                fs.Close();
            }
            //FilePath = "HighScore.txt";

            //temp = Path.Combine(projectPath, @"Data", @"temp.txt");
            //StreamReader sr = new StreamReader(FilePath);

        }

        public String GetPlayerName()
        {
            return PlayerName;
        }

        public void SetHighScore(int playerScore)
        {
            //reads all lines from file
            string[] lines = File.ReadAllLines(FilePath);

            //finds the correct place to insert the highscore
            int placeToInsertScore = lines.Length;
            for (int i = 0; i < lines.Length; i++)
            {
                int score = Int32.Parse(lines[i].Split(',')[1]); //finds the correct location for the incoming score
                if (playerScore > score)
                {
                    placeToInsertScore = i;//stores correct location
                    break;
                }
            }

            if (placeToInsertScore < numHighscoresToDisplay) // if the new high score is in top 10
            {
                int size = Math.Min(numHighscoresToDisplay, lines.Length + 1);//gets the lower of the two
                string[] newLines = new string[size];

                for (int i = 0; i < placeToInsertScore; i++)
                {
                    newLines[i] = lines[i];//returns original places before changedw
                }

                newLines[placeToInsertScore] = (PlayerName + "," + playerScore);//inserts the new score at the correct location


                for (int i = placeToInsertScore + 1; i < size; i++)
                {
                    newLines[i] = lines[i - 1];//shifts the remainder back and the last line is pushed down the list and not looked at
                }

                //Rewrites line to new file
                //File.Delete(FilePath);
                File.WriteAllLines(FilePath, newLines);
            }
        }

        public List<Player> getHighscores()
        {
            List<Player> players = new List<Player>();
            StreamReader sr = new StreamReader(FilePath);

            for (int i = 0; i < players.Count; i++)
            {
                string line = sr.ReadLine();//reads each line
                string name = line.Split(',')[0];//takes the name before the comma 
                string score = line.Split(',')[1];//takes the highscore after the comma
                Player player = new Player(name, Int32.Parse(score));//takes the name and makes the string score into an int

                players[i] = player;
            }

            return players;
        }
    }

}
