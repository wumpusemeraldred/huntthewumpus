﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntTheWumpus.Battle.Enums
{
    enum Item
    {
        //Crafting Items:
        WOOD,
        STONE,
        LEATHER,


        //Healing Items:
        APPLE, //heals small
        MYSTERY_MEAT, // heals med
        CHEESE, //heals large

        CHOCOLATE, //heals party small
        COOKIES, //heals party med
        PIZZA, //heals party large

        PROTEIN_SHAKE, //buffs attack max
        REDDISH_BULL,//buffs specialAttack max
        COOL_AID, //buffs accuracy max
        SIX_HOUR_ENERGY, //buffs evade max

        COFFEE, //Revive, or give auto-revive status
        MILK, // cleanses debuffs
        HOLY_WATER, //cleanses negative status effects

        ROCK_CANDY, // buffs entire party's defense
        POP_STONES, //takes some damage, buffs attack, special attack, and accuracy

    }
}
