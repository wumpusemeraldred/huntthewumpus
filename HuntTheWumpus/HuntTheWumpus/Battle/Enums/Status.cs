﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntTheWumpus.Battle.Enums
{
    enum Status
    {
        BURN,
        FREEZE,
        POISON,
        STUN,
        BLEED,

        NONE

    }
}
