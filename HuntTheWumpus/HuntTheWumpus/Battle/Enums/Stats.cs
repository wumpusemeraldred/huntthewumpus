﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntTheWumpus.Battle.Enums
{
    enum Stats
    {
        ATTACK,
        SPECIAL_ATTACK,
        DEFENSE,
        SPECIAL_DEFENSE,
        ACCURACY, 
        EVADE
    }
}
