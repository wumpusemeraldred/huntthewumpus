﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntTheWumpus.Battle.Enums
{
    enum Element
    {
        FIRE,
        ICE,
        WATER,
        TOXIN,
        EARTH,
        WIND,

        NONE,
    }
}