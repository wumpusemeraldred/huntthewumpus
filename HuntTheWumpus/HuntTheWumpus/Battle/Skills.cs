﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntTheWumpus.Battle.Entities;


namespace HuntTheWumpus.Battle
{
    class Skills //degrees and probablities are in percent
    {
        public Random gen = new Random();
        
        /*
        public Skills()
        {
            gen = new Random();
        }
        */
        

        //each move, besides healing, takes into account accuracy and evade to determine if the action connects or misses
        //if the attacker's accuracy is higher than the target's evade, the attack will always connect
        //if not, the chance the attack connects is (attacker's accuracy) / (target's evade)
        //THERE IS A CHANCE THAT THE ACCURACY CALCULATION IS CALLED FROM FOES RATHER THEN SKILLS
        public Boolean attackConncectChanceExecution(Entity attacker, Entity target)
        {
            int r = (int)(gen.NextDouble() * target.getFinalEvade()); //random value between 0 and target's evade
            if (r < attacker.getFinalAccuracy())                      // if accuracy is larger, attack always connects
            {                                                         // otherwise chance of accuracy/evade
                return true;
            }
            else return false;
        }

        //Physical Attack
        public void basicPhysicalAttack(int power, int element, int elementDegree, Entity attacker, Entity target)
        {
            if (this.attackConncectChanceExecution(attacker, target))
            {
                double elementBasedPortion = elementDegree / 100.0;             //Between 0.0 and 1.0. Separates the elemental part
                double nonElementBasedPortion = (100 - elementDegree) / 100.0;  // from non-elemental part for damage calculation

                double baseDamage = power * attacker.getFinalAttack() / target.getFinalDefense();   //damage calculation algorithm

                double elementBasedDamage = elementBasedPortion * baseDamage * target.elementalResistance[element]; //take into account elemental resistance/weakness
                double nonElementBasedDamage = nonElementBasedPortion * baseDamage;

                double finalBaseDamage = elementBasedDamage + nonElementBasedDamage;

                double range = finalBaseDamage * 0.2;
                int r = (int)(gen.NextDouble() * range + 0.9 * finalBaseDamage); //gets damage amount
                target.changeHP(r);                                              //with 10% randomness
            }
            else
            {
                //miss animations
            }
        }
        //"Magic" Attack, only difference from above is that damage calculation uses the "special" stats
        public void basicSpecialAttack(int power, int element, int elementDegree, Entity attacker, Entity target)
        {
            if (this.attackConncectChanceExecution(attacker, target))
            {
                double elementBasedPortion = elementDegree / 100.0;
                double nonElementBasedPortion = (100 - elementDegree) / 100.0;

                double baseDamage = power * attacker.getFinalSpecialAttack() / target.getFinalSpecialDefense();   //uses special attack and defense instead

                double elementBasedDamage = elementBasedPortion * baseDamage * target.elementalResistance[element];
                double nonElementBasedDamage = nonElementBasedPortion * baseDamage;

                double finalBaseDamage = elementBasedDamage + nonElementBasedDamage;

                double range = finalBaseDamage * 0.2;
                int r = (int)(gen.NextDouble() * range + 0.9 * finalBaseDamage);
                target.changeHP(r);
            }
            else
            {
                //miss animations
            }
        }

        public void applyStatus(int status, int probability, int duration, Entity target)
        {
            int r = (int)(gen.NextDouble() * 100 / target.statusResistance[status]);
            if (r < probability)
            {
                target.statusDuration[status] += duration;
                if (target.statusDuration[status] > 9)
                {
                    target.statusDuration[status] = 9;
                }
            }
        }

        public void applyDebuff(int debuff, int probability, int severity, Entity target) //caps debuffs at 50%
        {
            int r = (int)(gen.NextDouble() * 100 / target.debuffResistance[debuff]);
            if (r < probability)
            {
                target.buffAmount[debuff] -= severity;
                if (target.buffAmount[debuff] < -50)
                {
                    target.buffAmount[debuff] = -50;
                }
            }
        }

        public void applyBuff(int buff, int severity, Entity target) //caps buffs at 50%
        {
            target.buffAmount[buff] += severity;
            if (target.buffAmount[buff] > 50)
            {
                target.buffAmount[buff] = 50;
            }
        }

        public void heal(int power, Entity healer, Entity target)
        {
            int baseHeal = power * healer.specialAttack; // heal amount based on specialAttack
            double range = baseHeal * 0.2;

            int r = -1 * (int)(gen.NextDouble() * range + 0.9 * baseHeal);   //makes r negative for healing
            target.changeHP(r);                                              //with 10% randomness
        }

        public void revive(int power, int autoReviveDuration, Entity healer, PlayerCharacter target)
        {
            if (target.getAlive())
            {
                //give auto-revive status
                target.statusPositiveDuration[autoReviveDuration]++;
            }
            else if (!target.getAlive())
            {
                int baseHeal = power * healer.specialAttack; // heal amount based on specialAttack
                double range = baseHeal * 0.2;

                int r = -1 * (int)(gen.NextDouble() * range + 0.9 * baseHeal);   //makes r negative for healing
                target.changeHP(r);
                target.canTakeTurn = true; //revived players can take their turn.

            }
        }

        public void cleanse(Entity target) //gets rid of negative effects
        {
            for (int i = 0; i < 6; i++)// maybe not 6, however many status effects there are
            {
                target.statusDuration[i] = 0;
            }
        }

        public void dispel(Entity target) // gets rid of positive effects
        {
            for (int i = 0; i < 6; i++)// maybe not 6, however many positive status effects there are
            {
                target.statusPositiveDuration[i] = 0;
            }
        }
    }
}

