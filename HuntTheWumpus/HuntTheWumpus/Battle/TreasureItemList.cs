﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntTheWumpus.Battle.Enums;

namespace HuntTheWumpus.Battle
{
    class TreasureItemList
    {
        int[][] itemList = new int[50][]; //not 50, however many chests there are
        public TreasureItemList()
        {
            //Every single chest and its contents are kept here

            //chest number
            itemList[0] = new int[] { (int)Item.COFFEE, (int)Item.COFFEE, (int)Item.COFFEE };
            itemList[1] = new int[] { (int)Item.APPLE, (int)Item.COOL_AID, (int)Item.CHOCOLATE };
            //itemList[2] = 

        }

        public int[] itemsInChest(int i) //transfers above items 
        {
            int[] finalContents = new int[50]; // not 50, number of items
            foreach(int item in itemList[i])
            {
                finalContents[item]++;
            }
            return finalContents;
        }
        
    }
}

