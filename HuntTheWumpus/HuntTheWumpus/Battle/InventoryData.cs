﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntTheWumpus.Battle.Entities.Players;
using HuntTheWumpus.Battle.Entities;

//maybe scrapped
using HuntTheWumpus.Battle.Equipment;
using HuntTheWumpus.Battle.Equipment.PaulEquipment.PaulWeapon;
using HuntTheWumpus.Battle.Equipment.PaulEquipment.PaulArmor;

namespace HuntTheWumpus.Battle
{
    class InventoryData
    {
        public int gold = 0;
        public int score = 0;
        public int[] craftingItems = new int[50]; //probably not 50
        public int[] healingItems = new int[12];  //same, except needs to subtract 50 or whatever the number is
        List<Weapon> weaponsList = new List<Weapon>();
        List<Armor> armorList = new List<Armor>();
        public PlayerCharacter[] players = new PlayerCharacter[3];

        public InventoryData()
        {
            //players
            Paul paul = new Paul();
            Emma emma = new Emma();
            Matthew matthew = new Matthew();

            players[0] = paul;
            players[1] = emma;
            players[2] = matthew;

            /*
            //starting weapons
            StormBow stormBow = new StormBow();

            //starting armors
            RangerGear rangerGear = new RangerGear();


            //Weapons arraylist
            weaponsList.Add(stormBow);

            //Armor arraylist
            armorList.Add(rangerGear);
            */

        }
        
        
    }
}
