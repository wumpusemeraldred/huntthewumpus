﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntTheWumpus.Battle.Enums;

namespace HuntTheWumpus.Battle.Entities.Foes
{
    class EarthElemental : Foe
    {
        public EarthElemental(int level) //also need the sprite location in parameter, 
                                      //depends on position in arraylist
        {
            name = "Earth Elemental";
            //foeSprite = need it for spawning, gets sprites

            maxHP = 200;
            HP = maxHP;
            attack = 8;
            defense = 20;
            specialAttack = 8;
            specialDefense = 20;
            accuracy = 10;
            evade = 5;

            EXP = 10;
            AP = 1;
            Gold = 10;
            score = 10;

            elementalResistance[(int)Element.FIRE] = 0.5;
            elementalResistance[(int)Element.TOXIN] = 0.5;
            elementalResistance[(int)Element.ICE] = 1.5;
            elementalResistance[(int)Element.WATER] = 1.5;

            statusResistance[(int)Status.FREEZE] = 1.0;

            possibleDrops = new int[5] { (int)Item.STONE, (int)Item.STONE, (int)Item.STONE, (int)Item.ROCK_CANDY, (int)Item.POP_STONES};
            correspondingDropPercentages = new int[5] { 90, 30, 10, 5, 5 };

        }

        //public override void initialize()
        // {

        //}

        public override void takeTurn()
        {

            int r = (int)(gen.NextDouble() * 3);

            if (r == 0)
            {
                //this.attack1();
            }
            if (r == 1)
            {
                //this.attack2();
            }
            if (r == 2)
            {
                this.attack3();
            }

        }

        public void attack1()
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.basicPhysicalAttack(20, (int)Element.NONE, 0, this, target/*somehow access possible players*/);
        }

        public void attack2() // stone throw
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.basicSpecialAttack(20, (int)Element.EARTH, 100, this, target);
        }

        public void attack3() // self defense buff
        {
            skills.applyBuff((int)Stats.DEFENSE, 25, this);
        }
    }
}
