﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntTheWumpus.Battle.Enums;

namespace HuntTheWumpus.Battle.Entities.Foes
{
    class Slime : Foe
    {
        public Slime(int level) //also need the sprite location in parameter, 
                                         //depends on position in arraylist
        {
            name = "Slime";
            //foeSprite = need it for spawning, gets sprites

            maxHP = 200;
            HP = maxHP;
            attack = 8;
            defense = 12;
            specialAttack = 8;
            specialDefense = 12;
            accuracy = 10;
            evade = 5;

            EXP = 100;
            AP = 1;
            Gold = 10;
            score = 10;

            elementalResistance[(int)Element.TOXIN] = -1.0;

            possibleDrops = new int[1] { (int)Item.HOLY_WATER };
            correspondingDropPercentages = new int[1] { 5 };

        }

        //public override void initialize()
        // {

        //}

        public override void takeTurn()
        {

            int r = (int)(gen.NextDouble() * 3);

            if (r == 0)
            {
                //this.attack1();
            }
            if (r == 1)
            {
                //this.attack2();
            }
            if (r == 2)
            {
                //this.attack3();
            }

        }

        public void attack1()
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.basicPhysicalAttack(20, (int)Element.NONE, 0, this, target);
        }

        public void attack2() // idk
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.basicSpecialAttack(20, (int)Element.TOXIN, 100, this, target);
        }

        public void attack3() // chance to poison
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.applyStatus((int)Status.POISON, 10, 3, target);
        }
    }
}
