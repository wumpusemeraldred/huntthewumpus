﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntTheWumpus.Battle.Enums;

namespace HuntTheWumpus.Battle.Entities.Foes
{
    class Lifeberg : Foe
    {
        public Lifeberg(int level) //also need the sprite location in parameter, 
                                         //depends on position in arraylist
        {
            name = "Lifeberg";
            //foeSprite = need it for spawning, gets sprites

            maxHP = 100;
            HP = maxHP;
            attack = 10;
            defense = 15;
            specialAttack = 10;
            specialDefense = 15;
            accuracy = 10;
            evade = 5;

            EXP = 100;
            AP = 1;
            Gold = 10;
            score = 10;

            elementalResistance[(int)Element.FIRE] = 1.5;
            elementalResistance[(int)Element.ICE] = 0.1;

            statusResistance[(int)Status.FREEZE] = 0.1;

            possibleDrops = new int[1] { (int)Item.HOLY_WATER };
            correspondingDropPercentages = new int[1] { 15 };

        }

        //public override void initialize()
        // {

        //}

        public override void takeTurn()
        {

            int r = (int)(gen.NextDouble() * 3);

            if (r == 0)
            {
                //this.attack1();
            }
            if (r == 1)
            {
                //this.attack2();
            }
            if (r == 2)
            {
                this.attack3();
            }

        }

        public void attack1()
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.basicPhysicalAttack(20, (int)Element.NONE, 0, this, target);
        }

        public void attack2() // ice shard
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.basicSpecialAttack(20, (int)Element.ICE, 100, this, target);
        }

        public void attack3() // Freeze 
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.applyStatus((int)Status.FREEZE, 25, 3, target);
        }
    }
}
