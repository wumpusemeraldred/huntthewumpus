﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntTheWumpus.Battle.Enums;

namespace HuntTheWumpus.Battle.Entities.Foes
{
    class CaveLion : Foe
    {
        public CaveLion(int level) //also need the sprite location in parameter, 
                                         //depends on position in arraylist
        {
            name = "Cave Lion";
            //foeSprite = need it for spawning, gets sprites

            maxHP = 150;
            HP = maxHP;
            attack = 25;
            defense = 25;
            specialAttack = 10;
            specialDefense = 10;
            accuracy = 10;
            evade = 5;

            EXP = 100;
            AP = 1;
            Gold = 100;
            score = 100;

            elementalResistance[(int)Element.FIRE] = 0.9;
            elementalResistance[(int)Element.TOXIN] = 0.9;
            elementalResistance[(int)Element.ICE] = 0.9;
            elementalResistance[(int)Element.WATER] = 0.9;
            elementalResistance[(int)Element.TOXIN] = 0.9;
            elementalResistance[(int)Element.WIND] = 0.9;

            statusResistance[(int)Status.STUN] = 0.5;

            possibleDrops = new int[4] { (int)Item.MYSTERY_MEAT, (int)Item.MYSTERY_MEAT, (int)Item.MYSTERY_MEAT, (int)Item.LEATHER };
            correspondingDropPercentages = new int[4] { 90, 80, 70, 70 };

        }

        //public override void initialize()
        // {

        //}

        public override void takeTurn()
        {

            int r = (int)(gen.NextDouble() * 3);

            if (r == 0)
            {
                //this.attack1();
            }
            if (r == 1)
            {
                //this.attack2();
            }
            if (r == 2)
            {
                this.attack3();
            }

        }

        public void attack1()
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.basicPhysicalAttack(50, (int)Element.NONE, 0, this, target/*somehow access possible players*/);
        }

        public void attack2() // Growl (debuffs attack)
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.applyDebuff((int)Stats.ATTACK, 80, 20, target);
        }

        public void attack3()
        {
            
        }
    }
}
