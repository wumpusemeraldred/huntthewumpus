﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntTheWumpus.Battle.Enums;

namespace HuntTheWumpus.Battle.Entities.Foes
{
    class AirElemental : Foe
    {
        public AirElemental(int level) //also need the sprite location in parameter, 
                                         //depends on position in arraylist
        {
            name = "Air Elemental";
            //foeSprite = need it for spawning, gets sprites

            maxHP = 80;
            HP = maxHP;
            attack = 10;
            defense = 8;
            specialAttack = 10;
            specialDefense = 10;
            accuracy = 20;
            evade = 20;

            EXP = 100;
            AP = 1;
            Gold = 10;
            score = 10;
            
            elementalResistance[(int)Element.ICE] = 1.5;
            elementalResistance[(int)Element.EARTH] = 0.0;

            possibleDrops = new int[1] { (int)Item.POP_STONES };
            correspondingDropPercentages = new int[1] { 15 };

        }

        //public override void initialize()
        // {

        //}

        public override void takeTurn()
        {

            int r = (int)(gen.NextDouble() * 2);

            if (r == 0)
            {
                //this.attack1();
            }
            if (r == 1)
            {
                //this.attack2();
            }

        }

        public void attack1()
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.basicPhysicalAttack(20, (int)Element.NONE, 0, this, target/*somehow access possible players*/);
        }

        public void attack2() // wind blast
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.basicSpecialAttack(20, (int)Element.WIND, 100, this, target);
        }
    }
}
