﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntTheWumpus.Battle.Enums;

namespace HuntTheWumpus.Battle.Entities.Foes
{
    class Edwin : Foe
    {
        public Edwin(int level) //also need the sprite location in parameter, 
                                        //depends on position in arraylist
        {
            name = "Smash God";
            //foeSprite = need it for spawning, gets sprites

            maxHP = 100;
            HP = maxHP;
            attack = 20;
            defense = 10;
            specialAttack = 20;
            specialDefense = 10;
            accuracy = 30;
            evade = 10;

            EXP = 100;
            AP = 1;
            Gold = 10;
            score = 10;

            possibleDrops = new int[2] { (int)Item.POP_STONES, (int)Item.ROCK_CANDY };
            correspondingDropPercentages = new int[2] { 50, 50 };

        }

        //public override void initialize()
        // {

        //}

        public override void takeTurn()
        {

            int r = (int)(gen.NextDouble() * 3);

            if (r == 0)
            {
                //this.attack1();
            }
            if (r == 1)
            {
                //this.attack2();
            }
            if (r == 2)
            {
                //this.attack3();
            }

        }

        public void attack1() // Lasers
        {
            PlayerCharacter target = this.getRandomTarget();
            for (int i = 0; i < 5; i++)
            {
                skills.basicPhysicalAttack(5, (int)Element.NONE, 0, this, target);
            }
        }

        public void attack2() // Wave-shine
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.basicSpecialAttack(5, (int)Element.NONE, 50, this, target);
            skills.applyStatus((int)Status.STUN, 50, 1, target);
        }

        public void attack3() // Fox Fire
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.basicSpecialAttack(25, (int)Element.FIRE, 100, this, target);
        }
    }
}
