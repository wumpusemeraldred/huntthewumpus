﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntTheWumpus.Battle.Enums;

namespace HuntTheWumpus.Battle.Entities.Foes
{
    class FireElemental : Foe
    {
        public FireElemental(int level) //also need the sprite location in parameter, 
                                         //depends on position in arraylist
        {
            name = "Fire Elemental";
            //foeSprite = need it for spawning, gets sprites

            maxHP = 150;
            HP = maxHP;
            attack = 20;
            defense = 10;
            specialAttack = 30;
            specialDefense = 10;
            accuracy = 10;
            evade = 5;

            EXP = 100;
            AP = 1;
            Gold = 10;
            score = 10;

            elementalResistance[(int)Element.FIRE] = -1.0;
            elementalResistance[(int)Element.ICE] = 0.5;
            elementalResistance[(int)Element.WATER] = 1.5;

            statusResistance[(int)Status.FREEZE] = 0.1;

            possibleDrops = new int[1] { (int)Item.POP_STONES };
            correspondingDropPercentages = new int[1] { 15 };

        }

        //public override void initialize()
        // {

        //}

        public override void takeTurn()
        {

            int r = (int)(gen.NextDouble() * 3);

            if (r == 0)
            {
                //this.attack1();
            }
            if (r == 1)
            {
                //this.attack2();
            }
            if (r == 2)
            {
                this.attack3();
            }

        }

        public void attack1()
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.applyStatus((int)Status.BURN, 50, 4, target);
        }

        public void attack2() // Fireball
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.basicSpecialAttack(20, (int)Element.FIRE, 100, this, target);
        }

        public void attack3() // self special buff
        {
            skills.applyBuff((int)Stats.SPECIAL_ATTACK, 25, this);
        }
    }
}
