﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntTheWumpus.Battle.Enums;

namespace HuntTheWumpus.Battle.Entities.Foes
{
    class SpookyGhost : Foe
    {
        public SpookyGhost(int level) //also need the sprite location in parameter, 
                                         //depends on position in arraylist
        {
            name = "Spooky Ghost";
            //foeSprite = need it for spawning, gets sprites

            maxHP = 25;
            HP = maxHP;
            attack = 10;
            defense = 10;
            specialAttack = 10;
            specialDefense = 10;
            accuracy = 10;
            evade = 25;

            EXP = 100;
            AP = 1;
            Gold = 10;
            score = 10;

            elementalResistance[(int)Element.NONE] = 0.0;

            possibleDrops = new int[1] { (int)Item.HOLY_WATER };
            correspondingDropPercentages = new int[1] { 15 };

        }

        //public override void initialize()
        // {

        //}

        public override void takeTurn()
        {

            int r = (int)(gen.NextDouble() * 2);

            if (r == 0)
            {
                //this.attack1();
            }
            if (r == 1)
            {
                //this.attack2();
            }


        }

        public void attack1() //basic attack
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.basicPhysicalAttack(20, (int)Element.NONE, 0, this, target);
        }

        public void attack2() // Spook
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.applyDebuff((int)Stats.ACCURACY, 25, 25, target);
        }

    }
}
