﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntTheWumpus.Battle.Enums;

namespace HuntTheWumpus.Battle.Entities.Foes
{
    class Wumpus : Foe
    {
        public Wumpus(int level) //also need the sprite location in parameter, 
                                 //depends on position in arraylist
        {
            name = "Wumpus";
            //foeSprite = need it for spawning, gets sprites

            maxHP = 500;
            HP = maxHP;
            attack = 20;
            defense = 20;
            specialAttack = 20;
            specialDefense = 20;
            accuracy = 20;
            evade = 20;

            EXP = 1000000;
            AP = 1;
            Gold = 1000000;
            score = 10;

            elementalResistance[(int)Element.FIRE] = 0.75;
            elementalResistance[(int)Element.TOXIN] = 0.75;
            elementalResistance[(int)Element.ICE] = 0.75;
            elementalResistance[(int)Element.WATER] = 0.75;
            elementalResistance[(int)Element.TOXIN] = 0.75;
            elementalResistance[(int)Element.WIND] = 0.75;

            statusResistance[(int)Status.STUN] = 0.5;
            statusResistance[(int)Status.FREEZE] = 0.5;
            statusResistance[(int)Status.POISON] = 0.5;
            statusResistance[(int)Status.BURN] = 0.5;
            
        }

        //public override void initialize()
        // {

        //}

        public override void takeTurn()
        {

            int r = (int)(gen.NextDouble() * 5);

            if (r == 0)
            {
                //this.attack1();
            }
            if (r == 1)
            {
                //this.attack2();
            }
            if (r == 2)
            {
                //this.attack3();
            }
            if (r == 3)
            {
                // this.attack4();
            }
            if (r == 4)
            {
                // this.attack5();
            }
        }

        public void attack1()
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.basicPhysicalAttack(50, (int)Element.NONE, 0, this, target);
        }

        public void attack2()
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.basicSpecialAttack(10, (int)Element.FIRE, 100, this, target);
            skills.basicSpecialAttack(10, (int)Element.WATER, 100, this, target);
            skills.basicSpecialAttack(10, (int)Element.ICE, 100, this, target);
            skills.basicSpecialAttack(10, (int)Element.EARTH, 100, this, target);
            skills.basicSpecialAttack(10, (int)Element.TOXIN, 100, this, target);
            skills.basicSpecialAttack(10, (int)Element.WIND, 100, this, target);
        }

        public void attack3()
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.applyDebuff((int)Stats.ATTACK, 100, 10, target);
            skills.applyDebuff((int)Stats.DEFENSE, 100, 10, target);
            skills.applyDebuff((int)Stats.SPECIAL_ATTACK, 100, 10, target);
            skills.applyDebuff((int)Stats.SPECIAL_DEFENSE, 100, 10, target);
        }

        public void attack4()
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.applyDebuff((int)Stats.EVADE, 100, 15, target);
        }

        public void attack5()
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.applyStatus((int)Status.BLEED, 25, 3, target);
            skills.applyStatus((int)Status.STUN, 25, 3, target);
            skills.applyStatus((int)Status.POISON, 25, 3, target);
        }
    }
}
