﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntTheWumpus.Battle.Enums;

namespace HuntTheWumpus.Battle.Entities.Foes
{
    class Mimic : Foe
    {
        public Mimic(int level) //also need the sprite location in parameter, 
                                      //depends on position in arraylist
        {
            name = "Mimic";
            //foeSprite = need it for spawning, gets sprites

            maxHP = 400;
            HP = maxHP;
            attack = 5;
            defense = 25;
            specialAttack = 5;
            specialDefense = 25;
            accuracy = 10;
            evade = 0;

            EXP = 10;
            AP = 1;
            Gold = 1000;
            score = 10;

            possibleDrops = new int[10] { (int)Item.HOLY_WATER, (int)Item.COOL_AID, (int)Item.POP_STONES, (int)Item.PROTEIN_SHAKE, (int)Item.REDDISH_BULL, (int)Item.ROCK_CANDY, (int)Item.SIX_HOUR_ENERGY, (int)Item.PIZZA, (int)Item.COFFEE, (int)Item.MILK, };
            correspondingDropPercentages = new int[10] { 70, 70, 70, 70, 70, 70, 70, 70, 70, 70 };
        }

        //public override void initialize()
        // {

        //}

        public override void takeTurn()
        {
            this.attack1();
        }

        public void attack1()
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.basicPhysicalAttack(10, (int)Element.NONE, 0, this, target);
        }
    }
}
