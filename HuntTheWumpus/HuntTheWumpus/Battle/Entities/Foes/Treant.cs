﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntTheWumpus.Battle.Enums;

namespace HuntTheWumpus.Battle.Entities.Foes
{
    class Treant : Foe
    {
        

        public Treant(int level) //also need the sprite location in parameter, 
                                      //depends on position in arraylist
        {
            name = "Treant";
            //foeSprite = need it for spawning, gets sprites

            maxHP = 100;
            HP = maxHP;
            attack = 15;
            defense = 10;
            specialAttack = 10;
            specialDefense = 10;
            accuracy = 10;
            evade = 5;

            EXP = 10;
            AP = 1;
            Gold = 10;
            score = 10;

            elementalResistance[(int)Element.FIRE] = 1.5;
            elementalResistance[(int)Element.TOXIN] = 1.5;
            elementalResistance[(int)Element.ICE] = 1.5;
            elementalResistance[(int)Element.WATER] = 0.25;

            //statusResistance[(int) Status.BURN] = 5.0;

            possibleDrops = new int[4] { (int)Item.WOOD, (int)Item.WOOD, (int)Item.WOOD, (int)Item.APPLE};
            correspondingDropPercentages = new int[4] { 90, 30, 10, 50};

        }

        //public override void initialize()
       // {
            
        //}

        public override void takeTurn()
        {

            int r = (int)(gen.NextDouble() * 3);

            if(r == 0)
            {
                this.attack1();
            }
            if (r == 1)
            {
                this.attack2();
            }
            if (r == 2)
            {
                this.attack3();
            }

        }

        public void attack1()
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.basicPhysicalAttack(10, (int)Element.NONE, 0, this, target);
        }

        public void attack2()
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.basicSpecialAttack(10, (int)Element.EARTH, 50, this, target);
        }

        public void attack3()
        {
            skills.heal(10, this, this);
        }
    }
}
