﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntTheWumpus.Battle.Enums;

namespace HuntTheWumpus.Battle.Entities.Foes
{
    class CaveBat : Foe
    {
        public CaveBat(int level) //also need the sprite location in parameter, 
                                         //depends on position in arraylist
        {
            name = "Cave Bat";
            //foeSprite = need it for spawning, gets sprites

            maxHP = 40;
            HP = maxHP;
            attack = 20;
            defense = 10;
            specialAttack = 10;
            specialDefense = 10;
            accuracy = 10;
            evade = 15;

            EXP = 10;
            AP = 1;
            Gold = 10;
            score = 10;
            
            elementalResistance[(int)Element.ICE] = 1.5;
            elementalResistance[(int)Element.WIND] = 1.5;
            elementalResistance[(int)Element.EARTH] = 0.0;

            possibleDrops = new int[2] { (int)Item.LEATHER, (int)Item.MYSTERY_MEAT };
            correspondingDropPercentages = new int[2] { 40, 40 };

        }

        //public override void initialize()
        // {

        //}

        public override void takeTurn()
        {

            int r = (int)(gen.NextDouble() * 3);

            if (r == 0)
            {
                //this.attack1();
            }
            if (r == 1)
            {
                //this.attack2();
            }
            if (r == 2)
            {
                //this.attack3();
            }

        }

        public void attack1()
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.basicPhysicalAttack(15, (int)Element.NONE, 0, this, target/*somehow access possible players*/);
        }

        public void attack2() // wing blast
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.basicSpecialAttack(15, (int)Element.WIND, 50, this, target);
        }

        public void attack3() // vampiric bite
        {
            PlayerCharacter target = this.getRandomTarget();
            skills.basicPhysicalAttack(10, (int)Element.NONE, 0, this, target);
            skills.heal(10, this, this);
        }
    }
}
