﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntTheWumpus.Battle.Enums;

namespace HuntTheWumpus.Battle.Entities.Players
{
    class Paul : PlayerCharacter
    {
        public static int EXP;
        public static int AP;
        public int level;
        
        public int[] levelOfskills = new int[4]; //number of skills, level, 0 means unavailable.
        //0: Really Sharp Arrow, avg dmg, good chance to apply bleed
        //1: Burning Arrow, 50% fire dmg, good chance to burn
        //2: Wind Slash, 100% wind magic
        //3: Cloak, buff target, increase evade stat


        public Paul(int Level)
        {
            level = Level;
            maxHP = 100;
            HP = maxHP;
            attack = 20;
            defense = 15;
            specialAttack = 10;
            specialDefense = 15;
            accuracy = 10;
            evade = 20;

            EXP = 0;
            AP = 0;

            entitySprite = new Sprite(".\\Images\\WalkingSpriteSheet.png", 20, 640 * 3, 32 * 3);
        }
        public Paul( )
        {
            level = 1;
            maxHP = 100;
            HP = maxHP;
            attack = 20;
            defense = 15;
            specialAttack = 10;
            specialDefense = 15;
            accuracy = 10;
            evade = 20;

            EXP = 0;
            AP = 0;
            entitySprite = new Sprite(".\\Images\\WalkingSpriteSheet.png", 20, 640 * 3, 32 * 3);
        }
        public override void basicAttack(Foe target)
        {
            //if(skills.attackConncectChanceExecution(this, target)) already part of basicAttack
            {
                skills.basicPhysicalAttack(30, (int)Element.NONE, 50, this, target);
            }
            
        }

        public void reallySharpArrow(Foe target)
        {
            skills.basicPhysicalAttack(20, (int)Element.WIND, 50, this, target);
            skills.applyStatus((int)Status.BLEED, 80, 3, target);
        }

        public void burningArrow(Foe target)
        {
            skills.basicPhysicalAttack(20, (int)Element.FIRE, 50, this, target);
            skills.applyStatus((int)Status.BLEED, 80, 3, target);
        }

        public void windSlash(Foe target)
        {
            skills.basicSpecialAttack(20, (int)Element.WIND, 100, this, target);
        }

        public void cloak()
        {
            skills.applyBuff((int)Stats.EVADE, 30, this);
        }

       // public override void initialize()


        public override void takeTurn()
        {
            //if (!this.getAlive())
            {
                return;
            }

            /*
            if ()//switch
            {

            }
            */

            //attack
            //command
            //skill
            //equip
            //item

        }
        public override Sprite getSprite()
        {
            return entitySprite;
        }

        public override void takeAction(int[] menuOutput) // logic tree, will be put in each player class
        {
            //CHANGE this.canTakeTurn TO FALSE UNLESS YOU SWITCH

            if (menuOutput[0] == 0)
            {
                this.basicAttack(foes[menuOutput[2]]);

            }
            else if (menuOutput[0] == 1)
            {
                if(menuOutput[1] == 0) //really sharp arrow
                {
                    this.reallySharpArrow(foes[menuOutput[2]]);
                }
                else if (menuOutput[1] == 1) //burning arrow
                {
                    this.burningArrow(foes[menuOutput[2]]);
                }
                else if (menuOutput[1] == 2) //wind slash
                {
                    this.windSlash(foes[menuOutput[3]]);
                }
                else if (menuOutput[1] == 3) //cloak
                {
                    this.cloak();
                }
            }
            else if (menuOutput[0] == 2)
            {

            }
            else if (menuOutput[0] == 3)
            {

            }
        }

    }
}
