﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntTheWumpus.Battle.Enums;

namespace HuntTheWumpus.Battle.Entities.Players
{
    class Emma : PlayerCharacter
    {
        public static int EXP;
        public static int AP;

        public int[] levelOfskills = new int[4]; //number of skills, level, 0 means unavailable.
        //0: heal party for a little hp
        //1: Summon: Ocean, magic, 100% Water, high dmg, hits all, debuffs enemy attack
        //2: Summon: Fallout, magic, 100% Poison, high dmg, hits one, high chance to poison
        //3: Summon: Hyperion Blast, magic, no type, very high dmg, stuns both Emma and target

        public Emma()
        {
            level = 1;
            maxHP = 100;
            HP = maxHP;
            attack = 10;
            defense = 15;
            specialAttack = 20;
            specialDefense = 15;
            accuracy = 15;
            evade = 15;

            EXP = 0;
            AP = 0;

            entitySprite = new Sprite(".\\Images\\WalkingSpriteSheet.png", 20, 640 * 3, 32 * 3);
        }
        public Emma(int Level)
        {
            level = Level;
        }
        public override void basicAttack(Foe target)
        {
            //if(skills.attackConncectChanceExecution(this, target)) already part of basicAttack
            {
                skills.basicPhysicalAttack(20, (int) Element.NONE, 50, this, target);
            }

        }

        public void heal()
        {
            skills.heal(20, this, this);
        }

        // public override void initialize()

        public void tsunami()
        {
            foreach(Foe f in foes)
            {
                skills.basicSpecialAttack(30, (int)Element.WATER, 100, this, f);
            }
        }

        public void fallout(Foe target)
        {
            skills.basicSpecialAttack(40, (int)Element.TOXIN, 100, this, target);
            skills.applyStatus((int)(Status.POISON), 80, 5, target);
        }

        public void hyperion(Foe target)
        {
            skills.basicSpecialAttack(50, (int)Element.NONE, 0, this, target);
            skills.applyStatus((int)(Status.STUN), 100, 1, target);
            skills.applyStatus((int)(Status.STUN), 100, 1, this);
        }


        public override void takeTurn()
        {
            //if (!this.getAlive())
            {
                return;
            }

            /*
            if ()//switch
            {

            }
            */

            //attack
            //command
            //skill
            //equip
            //item

        }
        public override Sprite getSprite()
        {
            return entitySprite;
        }

        public override void takeAction(int[] menuOutput) // logic tree, will be put in each player class
        {
            //CHANGE this.canTakeTurn TO FALSE UNLESS YOU SWITCH

            if (menuOutput[0] == 0)
            {
                this.basicAttack(foes[menuOutput[2]]);

            }
            else if (menuOutput[0] == 1)
            {
                if (menuOutput[1] == 0) //really sharp arrow
                {
                    this.heal();
                }
                else if (menuOutput[1] == 1) //burning arrow
                {
                    this.tsunami();
                }
                else if (menuOutput[1] == 2) //wind slash
                {
                   this.fallout(foes[menuOutput[3]]);
                }
                else if (menuOutput[1] == 3) //cloak
                {
                    this.hyperion(foes[menuOutput[3]]);
                }
            }
            else if (menuOutput[0] == 2)
            {

            }
            else if (menuOutput[0] == 3)
            {

            }
        }

    }
}
