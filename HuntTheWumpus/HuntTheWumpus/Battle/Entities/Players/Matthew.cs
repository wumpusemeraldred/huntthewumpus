﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntTheWumpus.Battle.Entities.Players
{
    class Matthew : PlayerCharacter
    {
        public static int EXP;
        public static int AP;

        public int[] levelOfskills = new int[4]; //number of skills, level, 0 means unavailable.
        //0: Tank Up, self buff def by 25%
        //1: Shield Bash, med dmg, high chance to stun
        //2: Earthquake, 80% earth, med dmg, hits all, debuffs evade, chance to stun
        //4: Taunt, apply debuff, lower enemy defense stat


        public Matthew()
        {
            level = 1;
            maxHP = 150;
            HP = maxHP;
            attack = 10;
            defense = 20;
            specialAttack = 10;
            specialDefense = 20;
            accuracy = 20;
            evade = 10;

            EXP = 0;
            AP = 0;

            entitySprite = new Sprite(".\\Images\\WalkingSpriteSheet.png", 20, 640 * 3, 32 * 3);
        }
        public Matthew(int Level)
        {
            level = Level;
        }
        public override void basicAttack(Foe target)
        {
            //if(skills.attackConncectChanceExecution(this, target)) already part of basicAttack
            {
                skills.basicPhysicalAttack(20, 0/*gets element of equipped weapon*/, 50, this, target);
            }

        }

        public void snipe(Foe target)
        {
            skills.basicPhysicalAttack(20, 0/*gets element of equipped weapon*/, 50, this, target);
        }

        // public override void initialize()


        public override void takeTurn()
        {
            //if (!this.getAlive())
            {
                return;
            }

            /*
            if ()//switch
            {

            }
            */

            //attack
            //command
            //skill
            //equip
            //item

        }
        public override Sprite getSprite()
        {
            return entitySprite;
        }

        public override void takeAction(int[] menuOutput) // logic tree, will be put in each player class
        {
            //CHANGE this.canTakeTurn TO FALSE UNLESS YOU SWITCH

            if (menuOutput[0] == 0)
            {
                this.basicAttack(foes[menuOutput[2]]);

            }
            else if (menuOutput[0] == 1)
            {
                if (menuOutput[1] == 0) //really sharp arrow
                {
                    //this.reallySharpArrow(foes[menuOutput[3]]);
                }
                else if (menuOutput[1] == 1) //burning arrow
                {
                   // this.burningArrow(foes[menuOutput[3]]);
                }
                else if (menuOutput[1] == 2) //wind slash
                {
                    //this.windSlash(foes[menuOutput[3]]);
                }
                else if (menuOutput[1] == 3) //cloak
                {
                  //  this.cloak();
                }
            }
            else if (menuOutput[0] == 2)
            {

            }
            else if (menuOutput[0] == 3)
            {

            }
        }

    }
}
