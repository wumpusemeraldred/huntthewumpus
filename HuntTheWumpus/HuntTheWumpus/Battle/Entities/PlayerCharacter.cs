﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntTheWumpus.Battle.Equipment;



namespace HuntTheWumpus.Battle.Entities
{
   abstract class PlayerCharacter : Entity
    {
        //public int MP;
        //public int maxMP;
        //might not have a mana system

        /*
        public Weapon equippedWeapon;
        public Armor equippedArmor;
        */

        public PlayerCharacter[] players;
        public Foe[] foes;

        
        public int level;
        //public Boolean selected;

        //public Boolean alive = true;


        public void resetBuffsAndStatusEffects()//removes all buffs and status effects upon death
        {
            for(int i = 0; i < 6; i++) //maybe not 8, number of status effects
            {
                this.statusDuration[i] = 0;
            }
            for(int i = 0; i < 6; i++)
            {
                this.buffAmount[i] = 0;
            }
            
        }

        public Boolean getAlive()
        {
            if (this.HP != 0)
            {
                return true;
            }
            else return false;
        }

        



        public override void takeTurn()
        {
            /*
            if ()// user doesn't switch 
            {
                turnTaken becomes true
            }
            */

            //attack
            //command
            //skill
            //equip
            //item

        }
        public abstract void basicAttack(Foe target);

        

        public virtual Sprite getSprite()
        {
            return this.entitySprite;
        }

        public void setPlayers(PlayerCharacter[] players)
        {
            this.players = players;
        }

        public void setFoes(Foe[] foes)
        {
            this.foes = foes;
        }
        public abstract void takeAction(int[] menuOutput);

    }
}
