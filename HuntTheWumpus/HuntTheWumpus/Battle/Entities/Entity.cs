﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntTheWumpus.Battle.Enums;



namespace HuntTheWumpus.Battle.Entities
{
    abstract class Entity
    {
        public Skills Skills;
        //stats, defined uniquely for enemies, players have base stats adjusted by equips
        public int HP;
        public int maxHP;
        public int attack;
        public int specialAttack;
        public int defense;
        public int specialDefense;
        public int accuracy;
        public int evade;

        //fixed values, defined uniquely for enemies, players have resistances adjusted by equips
        public double[] elementalResistance = new double[7] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 }; //default values of 1.0
        public double[] statusResistance = new double[6] {1.0, 1.0, 1.0, 1.0, 1.0, 1.0};   // same as above
        public double[] debuffResistance = new double[6] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 }; // more default values

        //changes with gameplay in battle
        public int[] statusDuration = new int[6];
        public int[] buffAmount = new int[6]; //also indicates duration, goes up and down in multiples of 5
        public int[] statusPositiveDuration = new int[2]; //size needed

        public Skills skills = new Skills();
        public Boolean canTakeTurn = true;
        public Sprite entitySprite;

        //public abstract void initialize(); // Spawn in

        public abstract void takeTurn(); //pull up menu for player, randomize actions for enemies

        public void changeHP(int damage) //parameter is positive for damage, negative for healing
        {
            this.HP -= damage;
            //display number somehow
            if(this.HP < 0)//puts lower bound on health at 0
            {
                this.HP = 0;
                if(this.statusPositiveDuration[(int)StatsPostive.AUTO_REVIVE] != 0) //When an entity dies with auto-revive status, it revives with 30% max health
                {
                    this.changeHP((int)(-0.3 * this.maxHP)); 
                }
            }
            if (this.HP > this.maxHP)// puts upper bound on health at maxHP
            {
                this.HP = maxHP;
            }
        }

        public void updateBuffsAndStatusEffects()
        {
            for(int i = 0; i < 6; i++) // not 8, however many status effects there are
            {
                if(this.statusDuration[i] != 0) //if afflicted by status effect, duration goes down one turn
                {
                    this.statusEffectTrigger(i);
                    this.statusDuration[i]--;
                }
            }
            for(int i = 0; i < 6; i++) //buff and debuff percentages move toward 0, always are multiples of 5
            {
                if(this.buffAmount[i] > 0)
                {
                    this.buffAmount[i] -= 5;
                }
                else if (this.buffAmount[i] < 0)
                {
                    this.buffAmount[i] += 5;
                }
            }
            for(int i = 0; i < 2; i++) // not 8, how ever many positive stats there are
            {
                if (this.statusPositiveDuration[i] != 0) //if affected by positive status effect, duration goes down one turn
                {
                    this.positiveStatusEffectTrigger(i);
                    this.statusPositiveDuration[i]--;
                }
            }
        }

        public void statusEffectTrigger(int status)
        {
            if(status == (int)Status.FREEZE || status == (int)Status.STUN)
            {
                this.canTakeTurn = false; //frozen and stunned enemies can't take action
            } 
            if(status == (int)Status.BLEED) //bleed deals 3% of current health per turn
            {
                this.changeHP((int)(0.03 * this.HP)); 
            }
            if(status == (int)Status.BURN) //burn deals 5% of current health, affected by fire resistance/weakness
            {
                this.changeHP((int)(0.05 * this.HP * this.elementalResistance[(int)Element.FIRE]));
            }
            if(status == (int)Status.POISON) //burn deals 5% of current health, affected by toxin resistance/weakness
            {
                this.changeHP((int)(0.05 * this.HP * this.elementalResistance[(int)Element.TOXIN]));
            }
        }

        public void positiveStatusEffectTrigger(int status)
        {
            if(status == (int)StatsPostive.REGEN) //heals 25% of maximum health every turn
            {
                this.changeHP((int)(-0.25 * this.maxHP));
            }
        }

        //accessing stats, taking buffs/debuffs into account
        //MIGHT NEED TWO COPIES, one for playerCharacters and one for foes, in respective classes
        public int getFinalAttack()
        {
            return (int)(this.attack * (1 + (this.buffAmount[(int)Stats.ATTACK]) / 100.0)); //incorporates the percentage of buff into the stat
        }
        public int getFinalSpecialAttack()
        {
            return (int)(this.specialAttack * (1 + (this.buffAmount[(int)Stats.SPECIAL_ATTACK]) / 100.0));
        }
        public int getFinalDefense()
        {
            return (int)(this.defense * (1 + (this.buffAmount[(int)Stats.DEFENSE]) / 100.0));
        }
        public int getFinalSpecialDefense()
        {
            return (int)(this.specialDefense * (1 + (this.buffAmount[(int)Stats.SPECIAL_DEFENSE]) / 100.0));
        }
        public int getFinalAccuracy()
        {
            return (int)(this.accuracy * (1 + (this.buffAmount[(int)Stats.ACCURACY]) / 100.0));
        }
        public int getFinalEvade()
        {
            return (int)(this.evade * (1 + (this.buffAmount[(int)Stats.EVADE]) / 100.0));
        }



        public void displayHP()
        {
            //Console.WriteLine(this.HP + " / " + this.maxHP);
        }

        //public abstract void getLocation();

    }
}
