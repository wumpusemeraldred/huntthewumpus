﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntTheWumpus.Battle.Enums;
using HuntTheWumpus.Controllers.UI_Stuff;

namespace HuntTheWumpus.Battle.Entities
{
    abstract class Foe : Entity
    {
        public String name;
        

        public int EXP;
        public int AP;
        public int Gold;
        public int score;
        //public Skills skills = new Skills();
        public PlayerCharacter[] players;
        public Boolean alive = true;
        

        public int[] itemDrops = new int[50]; //not 50, total number of items

        public int[] possibleDrops;                 //defined uniquely for each enemy, which items can drop
        public int[] correspondingDropPercentages;  //for each of those items, their drop percent, respectively

        public Random gen = new Random();

        public int getEXP()
        {
            return EXP;
        }

        public int getAP()
        {
            return AP;
        }

        public int getGold()
        {
            return Gold;
        }

        public int getScore()
        {
            return score;
        }


        public void dropChanceExecution(int itemIndex, int percentChance, int[] itemDrops)
        {
            int r = (int) gen.NextDouble() * 100;
            if(r < percentChance)
            {
                itemDrops[itemIndex]++;
            }   
        }

        public int[] getFinalItemDrops()
        {
            return itemDrops;
        }
        
        public void disappear() //get rip of graphics, determine which items are dropped
        {
            for(int i = 0; i < possibleDrops.Length; i++)
            {
                dropChanceExecution(possibleDrops[i], correspondingDropPercentages[i], itemDrops);
            }
        }

        public void setPlayers(PlayerCharacter[] players)
        {
            this.players = players;
        }

        public PlayerCharacter getRandomTarget()
        {
            int index;
            do
            {
               index = gen.Next(1); //MUST FIX LATER
            }
            while(!players[index].getAlive()); //only targets alive players

            return players[index];
            
        }


    }
}
