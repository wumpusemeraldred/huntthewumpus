﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntTheWumpus.Battle.Entities.Foes;

namespace HuntTheWumpus.Battle.Entities
{
    class FoePartyList
    {
        Foe[][] partyList = new Foe[50][];
        public FoePartyList()
        {
            //Every single battle ever is kept here

            //Battle number 0
            partyList[0] = new Foe[]{new Treant(1), new Treant(2), new Treant(3) }; 
            partyList[1] = transfer(new int[]{0 , 0 , 0},1);
            partyList[2] = transfer(new int[] { 0, 0, 0 ,0},1);

        }

        public Foe[] getFoeParty(int i)
        {
            return partyList[i];
        }
        public Foe[] transfer(int[] intList,int level)
        {
            Foe[] foeParty = new Foe[intList.Length];
            for (int i = 0; i < intList.Length; i++) {
                if (intList[i]==0)
                {
                    foeParty[i]= new Treant(level);
                }
                if (intList[i] == 1)
                {
                    foeParty[i] = new FireElemental(level);
                }

            }

            return foeParty;
        }
    }
}
