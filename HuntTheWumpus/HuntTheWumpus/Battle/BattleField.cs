﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntTheWumpus.Battle.Entities;
using HuntTheWumpus.Battle.Enums;



namespace HuntTheWumpus.Battle
{
    class BattleField
    {
        private Foe[] foes;
        private PlayerCharacter[] players;
        //private Boolean[] playersAlive = new Boolean[3];
        private int[] droppedItems = new int[50];
        private Boolean[] playersCanTakeTurn = new Boolean[3];
        private int expEarned = 0;
        private int APEarned = 0;
        private int goldEarned = 0;
        private int scoreEarned = 0;

        private PlayerCharacter selectedPlayer;

        public BattleField(PlayerCharacter[] players, Foe[] foes)
        {
            this.foes = foes;
            this.players = players;

            foreach(PlayerCharacter p in players)
            {
               // p.initialize();
            }

            foreach(Foe f in foes)
            {
                //f.initialize();
            }

            this.commenceBattle();
        }
        public void action(int [] i)
        {

        }

        public void checkAlive() //check after each turn which enemies and players are alive
        {
            for (int i = 0; i < foes.Length; i++)
            {
                if(foes[i].HP == 0) //if the foe has died, get all the drops from this enemy
                {
                    foes[i].disappear(); //gets drops
                    for(int j = 0; j < foes[i].getFinalItemDrops().Length; j++) //adds drops to the total drops attached to this instance of Battlefield
                    {
                        droppedItems[j] += foes[i].getFinalItemDrops()[j]; 
                    }

                    //gets added scores for this instance
                    scoreEarned += foes[i].getScore();
                    expEarned += foes[i].getEXP();
                    APEarned += foes[i].getAP();
                    goldEarned += foes[i].getGold();
                    foes[i].alive = false; //enemy will be set to dead
                }
            }
            for(int i = 0; i < players.Length; i++)
            {
                if(!players[i].getAlive())
                {
                    players[i].resetBuffsAndStatusEffects();
                    //sprite change to dead
                }
            }
        }
        //Method changes
        //Check next turn{} return 
        //Take turn(menu output())

        public void commenceBattle() //Battle start
        {
            for(int i = 0; i < 3; i++) //initiates array such that every player can take their turn
            {
                this.playersCanTakeTurn[i] = players[i].canTakeTurn;
            }

            foreach(Foe f in foes) //gives all foes the player array
            {
                f.setPlayers(players); 
            }

            foreach(PlayerCharacter p in players) // gives all players the player array and foe list
            {
                p.setPlayers(players);
                p.setFoes(foes);
            }


            while (!(allFoesDead()) || !allPlayersDead())
            {
                foreach(PlayerCharacter p in players) //update buffs and status effects
                    {
                        if (p.getAlive())
                        {
                            p.updateBuffsAndStatusEffects();
                        }
                    }

                while (!playerTurnOver()) //player turn is not over until ever character has taken their turn
                {
                    
                    for(int i = 0; i < players.Length; i++) //players given their turns
                    {
                        if (playersCanTakeTurn[i] && players[i].getAlive()) // if they haven't already taken their turn,
                        {                                             // i. e. switched, take turn now
                            selectedPlayer = players[i];
                            //timer goes to menu
                            //when final menu option finishes,
                            //selectedPlayer.takeAction(/*the output of the menu*/);
                            this.checkAlive();
                        }
                    }
                } //player turn over

                foreach (Foe f in foes) //update buffs and status effects for all enemies
                {
                    f.updateBuffsAndStatusEffects();
                }

                foreach(Foe f in foes) //enemies take turn
                {
                    f.takeTurn();
                    this.checkAlive();
                }

                

                for(int i = 0; i < players.Length; i++) //reloads turn taken array so every player that's alive can take a turn
                {                                        //unless they're dead
                    if (players[i].getAlive())                 //if revived mid-turn, boolean will be reverted in Skills.revive
                    {
                        players[i].canTakeTurn = true;
                    }
                }

                foreach(Foe f in foes)
                {
                    f.canTakeTurn = true; //to reset stuns and freezes
                }
            } //when while loop ends, battle is over

            if (allFoesDead())
            {
                this.endBattle();
            } else if (allPlayersDead())
            {
                this.gameOver();
            }
        }

        public void endBattle() //add dropped items, exp, AP, and gold to your collection
        {
            //show graphics, everything
        }
        public int getEXP()
        {
            return expEarned;
        }

        public int getAP()
        {
            return APEarned;
        }

        public int getGold()
        {
            return goldEarned;
        }

        public int getScore()
        {
            return scoreEarned;
        }

        public int[] getItemsDropped()
        {
            return droppedItems;
        }

        public void gameOver()
        {

        }

        public Boolean playerTurnOver() //checks if all live players have taken their turn
        {
            foreach(Boolean b in playersCanTakeTurn)
            {
                if (b)
                {
                    return false;
                }
            }
            return true;
        }

        public Boolean allPlayersDead() //checks if all players are alive
        {
            for(int i = 0; i < players.Length; i++)
            {
                if (players[i].getAlive())
                {
                    return false;
                }
            }
            return true;
        }

        public Boolean allFoesDead() //checks if all enemies are alive
        {
            for (int i = 0; i < foes.Length; i++)
            {
                if (foes[i].alive)
                {
                    return false;
                }
            }
            return true;
        }

        public Entity checkNextTurn() // returns who takes the next turn
        {
            for(int i = 0; i < playersCanTakeTurn.Length; i++)
            {
                if (playersCanTakeTurn[i])
                {
                    return players[i];
                }
            }
            return foes[0];
        }

       

    }
}
