﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntTheWumpus.Battle.Enums;

namespace HuntTheWumpus.Battle.Equipment
{
    abstract class Equips
    {
        
        public int level;
        public String name;

        public double[] HPModifier = new double[3];
        public double[] attackModifier = new double[3];
        public double[] defenseModifier = new double[3];
        public double[] specialAttackModifier = new double[3];
        public double[] specialDefenseModifier = new double[3];
        public double[] accuracyModifier = new double[3];
        public double[] evadeModifier = new double[3];

        public int[][] itemsForUpgrades = new int[2][] { new int[50], new int[50] }; 

        public Boolean checkAffordable(int[] availableItems)
        {
            int[] neededItems = this.itemsForUpgrades[this.level];
            for(int i = 0; i < 50; i++) //not 50, how ever many items there are
            {
                if(availableItems[i] < neededItems[i])
                {
                    return false;
                }
            }
            return true;
        }

        public void upgrade(int[] availableItems)
        {
            int[] neededItems = this.itemsForUpgrades[this.level];
            if (this.checkAffordable(availableItems))
            {
                for(int i = 0; i < 50; i++)
                {
                    availableItems[i] -= neededItems[i];
                }
            }

            this.level++;
        }
    }
}
