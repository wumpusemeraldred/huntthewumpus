﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntTheWumpus.Battle.Enums;
using HuntTheWumpus.Battle.Equipment;


namespace HuntTheWumpus.Battle.Equipment.PaulEquipment.PaulWeapon
{
    class StormBow : Weapon
    {
        public StormBow()
        {
            element = (int)Element.WIND;
        }
        
    }
}
